<%-- 
    Document   : login
    Created on : 26-abr-2019, 21:52:37
    Author     : COMPUTER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Iniciar Session Anco</title>

        <link href="stylesheets/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="stylesheets/responsive.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/sb-admin-2.js" type="text/javascript"></script>
        <link href="stylesheets/sb-admin-2.css" rel="stylesheet" type="text/css"/>
        <link href="stylesheets/metisMenu.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/metisMenu.min.js" type="text/javascript"></script>

        <!-- Modernizr -->
        <script src="js/modernizr.custom.56918.js"></script>
        <script src="js/Ajax.js" type="text/javascript"></script>       
        <link type="text/css" href="CSS/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>       
        <script src="js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script> 
        <script src="js/IniciarSession.js" type="text/javascript"></script>        

        <link href="stylesheets/grass-green.css" rel="stylesheet"/>

    </head>
    <body>
            
        <div id="loginModal" class="modal " tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
             aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3 id="loginModalLabel"><span class="light">Iniciar</span> Sesion Administrador</h3>
            </div>
            <div class="modal-body">
                <form role="form" action="#"/>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputEmail">Usuario</label>
                    <div class="controls">
                        <input type="text" class="input-block-level" id="inputEmail" placeholder="E-mail" name="txt_usuario" type="text" autofocus/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputPassword">Contraseña</label>
                    <div class="controls">
                        <input type="password" class="input-block-level" id="inputPassword" placeholder="Password" name="txt_clave"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox"/>
                            Recordarme me
                        </label>
                    </div>
                </div>
                <button type="button" class="btn btn-primary input-block-level bold higher" onclick="validarLogueoUsuario();">
                    INGRESAR
                </button>
                </form>
                <p class="center-align push-down-0">
                    <a href="#" data-dismiss="modal">Forgot your password?</a>
                </p>
            </div>
        </div>
        <!--Fin del formulario-->
    </body>
</html>
