var request = Ajax.getXMLHTTPRequest();
var id_noticia=null;

$(function () {
    $('#dialog_info').dialog({
        modal: true,
        autoOpen: false,
        width: 300,
        buttons: {
            "Aceptar": function (event, ui) {
                if(id_noticia!=null){
                    eliminarNoticia(id_noticia); 
                }
            },
            "Cancelar":function(){
                $(this).dialog("close");
            }
        },
        close: function( event, ui ) {            
        }
    });   
});
function eliminarNoticia(id) {
    var servlet;
    servlet = "../SRegistrarNoticia?op=eliminar_noticia&id_noticia=" + id;
    request.open("GET", servlet, true);
    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                var response = request.responseText;
                var response_event = eval(response);
                var respt = response_event.respt;
                var msg = response_event.msg;
                if (respt.indexOf("exito") >= 0) {
                    //resetComponent();
                    location.href = "registrarNoticia.jsp";
                } else if (respt.indexOf("error") >= 0) {
                    location.href = "registrarNoticia.jsp";
                }
            }
        } catch (err) {
            alert('Error al eliminar');
        }
    }
    request.send(null);
}
function mostrarDialogNoticia(id){
    id_noticia=id;
    $('#dialog_info_msg').append("¿Esta seguro de eliminar la noticia?");
    $('#dialog_info').dialog("open");
}

