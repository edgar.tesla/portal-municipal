

<%-- 
    Document   : mapa_distrito
    Created on : 22-abr-2019, 14:55:26
    Author     : COMPUTER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Administracion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content="ProteusThemes"/>

        <!--  Google Fonts  -->
        <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek'
              rel='stylesheet' type='text/css'/>

        <!-- Twitter Bootstrap -->
        <link href="../stylesheets/bootstrap.css" rel="stylesheet"/>
        <link href="../stylesheets/responsive.css" rel="stylesheet"/>
        <!-- Slider Revolution -->
        <link rel="stylesheet" href="../js/rs-plugin/css/settings.css" type="text/css"/>
        <!-- jQuery UI -->
        <link rel="stylesheet" href="../js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="../js/prettyphoto/css/prettyPhoto.css" type="text/css"/>
        <!-- main styles -->
        <link href="../stylesheets/grass-green.css" rel="stylesheet"/>
        <link href="../stylesheets/muni.css" rel="stylesheet"/>


        <!-- Modernizr -->
        <script src="../js/modernizr.custom.56918.js"></script>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" href="../images/muni_anco.jpg"/>
        <link rel="shortcut icon" href="../images/muni_anco.jpg"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>


    <body>


        <!--  ==========  -->
        <!--  = Header =  -->
        <!--  ==========  -->
        <header id="header">
            <div class="container">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Logo =  -->
                    <!--  ==========  -->
                    <div class="span7">
                        <a class="brand" href="index.html">
                            <img src="../images/muni_anco.jpg" alt="Webmarket Logo" width="48" height="48"/>
                            <span class="pacifico">Municipalidad</span>
                            <span class="tagline">Distrital Anco La Mar VRAEM 2019 - 2022</span>
                        </a>
                    </div>

                    <!--  ==========  -->
                    <!--  = Social Icons =  -->
                    <!--  ==========  -->
                    <div class="span5">
                        <div class="top-right">

                            <div class="register">
                                <a href="SLogout" role="button" data-toggle="modal">Cerrar Sesion</a>
                            </div>
                        </div>
                    </div> <!-- /social icons -->
                </div>
            </div>
        </header>

        <!--  ==========  -->
        <!--  = Breadcrumbs =  -->
        <!--  ==========  -->
        <div class="darker-stripe">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.jsp">Municipalidad</a>
                            </li>
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="mapa_distrito.jsp">Administracion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  ==========  -->
        <!--  = Main container =  -->
        <!--  ==========  -->
        <div class="container">
            <div class="push-up top-equal blocks-spacer">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Main content =  -->
                    <!--  ==========  -->
                    <section class="span8 single single-post" >
                        <div class="adminIcon">
                            
                            <a href="registrar_Convocatoria.jsp"><i class="fas fa-archive fa-10x" ></i>Registrar Convocatoria</a>
                        </div>
                        <div class="adminIcon">
                            
                            <a href="administrar_Convocatoria.jsp"><i class="fas fa-file-audio fa-10x"></i>administrar convocatoria</a>
                        </div>
                        <div class="adminIcon">
                            
                            <a href="registrarNoticia.jsp"><i class="fas fa-pencil-alt fa-10x"></i>Administrar Noticia</a>
                        </div>
                        <div class="adminIcon">
                            
                            <a href="NuevaNoticia.jsp"><i class="fas fa-pencil-alt fa-10x"></i>Nueva Noticia</a>
                        </div>

                        
                        
                        
                    </section> <!-- /main content -->

                    <!--  ==========  -->
                    <!--  = Sidebar =  -->
                    <!--  ==========  -->


                </div>
            </div>
        </div> <!-- /container -->

        <!--  ==========  -->
        <!--  = Brands Carousel =  -->
        <!--  ==========  -->



        <!--  ==========  -->
        <!--  = Footer =  -->
        <!--  ==========  -->
        <footer>

            <!--  ==========  -->
            <!--  = Bottom Footer =  -->
            <!--  ==========  -->
            <div class="foot-last">
                <a href="#" id="toTheTop">
                    <span class="icon-chevron-up"></span>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            &copy; Copyright 2019
                        </div>
                        <div class="span6">
                            <div class="pull-right">Municipalidad Distrital<a href="http://www.proteusthemes.com"> Anco La Mar VRAEM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /bottom footer -->
        </footer> <!-- /footer -->


        <!--  ==========  -->
        <!--  = Modal Windows =  -->
        <!--  ==========  -->




        <!--  ==========  -->
        <!--  = JavaScript =  -->
        <!--  ==========  -->

        <!--  = FB =  -->

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>


        <!--  = jQuery - CDN with local fallback =  -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            if (typeof jQuery == 'undefined') {
                document.write('<script src="js/jquery.min.js"><\/script>');
            }
        </script>

        <!--  = _ =  -->
        <script src="js/underscore/underscore-min.js" type="text/javascript"></script>

        <!--  = Bootstrap =  -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

        <!--  = Slider Revolution =  -->
        <script src="js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
        <script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>

        <!--  = CarouFredSel =  -->
        <script src="js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>

        <!--  = jQuery UI =  -->
        <script src="js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.10.3/touch-fix.min.js" type="text/javascript"></script>

        <!--  = Isotope =  -->
        <script src="js/isotope/jquery.isotope.min.js" type="text/javascript"></script>

        <!--  = Tour =  -->
        <script src="js/bootstrap-tour/build/js/bootstrap-tour.min.js" type="text/javascript"></script>

        <!--  = PrettyPhoto =  -->
        <script src="js/prettyphoto/js/jquery.prettyPhoto.js" type="text/javascript"></script>

        <!--  = Google Maps API =  -->
        <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyDvMjN1g49P1MA2Onsj-GulDkmDuuH6aoU&amp;sensor=false"></script>
        <script type="text/javascript" src="js/goMap/js/jquery.gomap-1.3.2.min.js"></script>

        <!--  = Custom JS =  -->
        <script src="js/custom.js" type="text/javascript"></script>

    </body>
</html>
