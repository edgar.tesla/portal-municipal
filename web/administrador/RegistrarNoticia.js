function crearCargador(){
  new qq.FileUploader({
      element: document.getElementById('file_uploader'),
      action: '../SRegistrarNoticia?op=subir_fotografia_personal', 
      sizeLimit: 0, 
      minSizeLimit: 0, 
      allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'], 
      onComplete: function(id, fileName, responseJSON){ 
           try{
                var request = null;
                     try{request = new XMLHttpRequest();}
                     catch(err1){
                         try{request = new ActiveXObject("Msxml2.XMLHTTP");}
                         catch(err2){
                             try{request = new ActiveXObject("Microsoft.XMLHTTP");}
                             catch(err3){request = null;}
                         }
                     }
                var ahora = new Date();
                var id_request = Math.abs(Math.sin(ahora.getTime()));     
                var servlet;      
                servlet = "../SRegistrarNoticia?op=previsualizar_fotografia_personal&id="+id_request;  
                request.open("GET",servlet,true);
                request.onreadystatechange = function(){
                try{                   
                       if(request.readyState == 4){ 
                         var response = request.responseText; 
                         var datos_proveedor = eval(response);
                         document.getElementById("fotografia_noticia").src=datos_proveedor.url_fotografia_personal;                              
                       }
                   } catch(err){
                     alert(err);
                   }
                 }
                 request.send(null);                
           }catch(ex){
                alert(ex);
           }
      },
      onProgress: function(id, fileName, loaded, total){},
      onCancel: function(id, fileName){}, 
     debug: false
  });
}

$(function () {
    $('#dialog_info').dialog({
        modal: true,
        autoOpen: false,
        width: 300,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("close");
                location.href = "registrarNoticia.jsp";
            }
        },
        close: function( event, ui ) {
            
            location.href = "registrarNoticia.jsp";
        }
    });
    $('#dialog_alert').dialog({
        modal: true,
        autoOpen: false,
        width: 300,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });
    $('#dialog_alert_validacion').dialog({
        modal: true,
        autoOpen: false,
        width: 300,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });
});
var request = Ajax.getXMLHTTPRequest();
function registrarNoticia() {
    var txt_titulo = document.getElementsByName("txt_titulo").item(0);
    var txt_especificacion = document.getElementsByName("txt_especificacion").item(0);
    var editar = document.getElementsByName("editar").item(0);
    //var im_noticia = document.getElementsByName("im_noticia").item(0);
    if (txt_titulo.value.length <= 0
            | txt_especificacion.value.length <= 0
            ) {
        $('#dialog_alert_validacion').dialog('open');
    } else {
        var ahora = new Date();
        var id = Math.abs(Math.sin(ahora.getTime()));
        var servlet;
        servlet = "../SRegistrarNoticia?op=registrar_personal&id=" + id;
        servlet += "&txt_titulo=" + txt_titulo.value;
        servlet += "&txt_especificacion=" + txt_especificacion.value;
        servlet += "&editar=" + editar.value;
        //servlet += "&im_noticia=" + im_noticia.value;
        //alert('hola :'+im_noticia.value);
        request.open("GET", servlet, true);
        request.onreadystatechange = function () {
            try {
                if (request.readyState == 4) {
                    var response = request.responseText;
                    var response_event = eval(response);
                    var respt = response_event.respt;
                    var msg = response_event.msg;
                    if (respt.indexOf("exito") >= 0) {
                        resetComponent();
                        $('#dialog_info_msg').append(msg);
                        $('#dialog_info').dialog('open');
                        
                    } else if (respt.indexOf("error") >= 0) {
                        $('#dialog_alert_msg').append(msg);
                        $('#dialog_alert').dialog('open');
                    }
                }
            } catch (err) {
                console.log(err);
                $('#dialog_alert_msg').append("Por Favor Primero Seleccione Una Fotografia.");
                $('#dialog_alert').dialog('open');
            }
        }
        resetComponent();
        request.send(null);
        
    }
}



function validarFotografiaSubida() {
    var ahora = new Date();
    var id = Math.abs(Math.sin(ahora.getTime()));
    var servlet;
    servlet = "../SRegistrarNoticia?op=fotografia_subida&id=" + id;
    request.open("GET", servlet, true);
    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                var response = request.responseText;
                var response_event = eval(response);
                var respt = response_event.respt;
                var msg = response_event.msg;
                if (respt.indexOf("no") >= 0) {
                    $('#dialog_alert_validacion_msg').append(msg);
                    $('#dialog_alert_validacion').dialog('open');
                } else if (respt.indexOf("si") >= 0) {

                }
            }
        } catch (err) {
            alert(err);
        }
    }
    request.send(null);
}
function resetComponent() {
    document.getElementsByName("txt_titulo").item(0).value = "";
    document.getElementsByName("txt_especificacion").item(0).value = "";
    document.getElementById("fotografia_noticia").src = "../images/logo.png";
}
