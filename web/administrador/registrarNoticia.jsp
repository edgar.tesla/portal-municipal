<%-- 
    Document   : registrarNoticia
    Created on : 23-abr-2019, 7:41:43
    Author     : COMPUTER
--%>

<%@page import="gob.muni.bean.BNoticia"%>
<%@page import="gob.muni.bl.BLNoticia"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Administracion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content="ProteusThemes"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <!--  Google Fonts  -->
        <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek'
              rel='stylesheet' type='text/css'/>

        <!-- Twitter Bootstrap -->
        <link href="../stylesheets/bootstrap.css" rel="stylesheet"/>
        <link href="../stylesheets/responsive.css" rel="stylesheet"/>

        <!-- Slider Revolution -->
        <link rel="stylesheet" href="../js/rs-plugin/css/settings.css" type="text/css"/>
        <!-- jQuery UI -->
        <link rel="stylesheet" href="../js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="../js/prettyphoto/css/prettyPhoto.css" type="text/css"/>
        <!-- main styles -->
        <link href="../stylesheets/grass-green.css" rel="stylesheet"/>
        <link href="../stylesheets/muni.css" rel="stylesheet"/>
        <link type="text/css" href="../CSS/jquery-ui-1.8.23.custom.css" rel="stylesheet" />



        <!-- Modernizr -->
        <script src="../js/modernizr.custom.56918.js"></script>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" href="../images/muni_anco.jpg"/>
        <link rel="shortcut icon" href="../images/muni_anco.jpg"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <style>
            .table-responsive>tbody>tr:nth-child(odd)>td, 
            .table-responsive>tbody>tr:nth-child(odd)>th {
                background-color:white;
            }
            .table-responsive>tbody>tr:nth-child(even)>td, 
            .table-responsive>tbody>tr:nth-child(even)>th {
                background-color: white;
            }
            .table-responsive>thead>tr>th {
                background-color: #95c538;
                color: white;
            }
        </style>
    </head>


    <body >


        <!--  ==========  -->
        <!--  = Header =  -->
        <!--  ==========  -->
        <header id="header">
            <div class="container">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Logo =  -->
                    <!--  ==========  -->
                    <div class="span7">
                        <a class="brand" href="index.jsp">
                            <img src="../images/muni_anco.jpg" alt="Webmarket Logo" width="48" height="48"/>
                            <span class="pacifico">Municipalidad</span>
                            <span class="tagline">Distrital Anco La Mar VRAEM 2019 - 2022</span>
                        </a>
                    </div>

                    <!--  ==========  -->
                    <!--  = Social Icons =  -->
                    <!--  ==========  -->
                    <div class="span5">
                        <div class="top-right">

                            <div class="register">
                                <a href="SLogout" >Cerrar Sesion</a>
                            </div>
                        </div>
                    </div> <!-- /social icons -->
                </div>
            </div>
        </header>

        <!--  ==========  -->
        <!--  = Breadcrumbs =  -->
        <!--  ==========  -->
        <div class="darker-stripe">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.jsp">Municipalidad</a>
                            </li>
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="mapa_distrito.jsp">Administracion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  ==========  -->
        <!--  = Main container =  -->
        <!--  ==========  -->
        <div class="container">
            <div class="push-up top-equal blocks-spacer">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Main content =  -->
                    <!--  ==========  -->
                    <section class="span8 single single-post" >
                        <center>
                            <h1>Administar Noticia</h1>
                        </center>

                        <%
                            BLNoticia emp = new BLNoticia(request);
                            BNoticia imgvo = new BNoticia();
                            ArrayList<BNoticia> listar = emp.listarNoticia();
                        %>
                        <a href="NuevaNoticia.jsp" class="caption lfl btn btn-primary btn_theme" data-x="120">
                            Nueva Noticia
                        </a>
                        <a href="admin.jsp" class="caption lfl btn btn-primary btn_theme" data-x="120">
                            Regresar
                        </a>
                        <hr/>
                        <table class="table table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>TITULO</th>
                                    <th>ESPECIFICACION</th>
                                    <th>EDITAR</th>
                                    <th>ELIMINAR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%if (listar.size() > 0) {
                                        for (BNoticia listar2 : listar) {
                                            imgvo = listar2;
                                %>
                                <tr>
                                    <td><%=imgvo.getCd_noticia()%></td>
                                    <td><%=imgvo.getDs_titulo()%></td>
                                    <td><%=imgvo.getDs_especificacion()%></td>
                                    <td>                            
                            <center><a href="NuevaNoticia.jsp?id=<%=imgvo.getCd_noticia()%>" class="caption lfl btn btn-primary btn_theme">Editar</a></center>                            
                            </td>
                            <td>
                            <center><a onclick="mostrarDialogNoticia(<%=imgvo.getCd_noticia()%>)" class="caption lfl btn btn-secondary btn_theme">Eliminar</a></center>
                            </td>
                            </tr>
                            <%}
                                }%>
                            </tbody>
                        </table>
                    </section> <!-- /main content -->
                    <div id="dialog_info" title="Atencion" class="alert">            
                        <div id="dialog_info_msg" class="info">                    
                            <img src="../Recursos/icon/Info.png" width="12" height="12" alt="Alert"/> 
                        </div>               
                    </div>
                    <!--  ==========  -->
                    <!--  = Sidebar =  -->
                    <!--  ==========  -->


                </div>
            </div>
        </div> <!-- /container -->

        <!--  ==========  -->
        <!--  = Brands Carousel =  -->
        <!--  ==========  -->



        <!--  ==========  -->
        <!--  = Footer =  -->
        <!--  ==========  -->
        <footer>

            <!--  ==========  -->
            <!--  = Bottom Footer =  -->
            <!--  ==========  -->
            <div class="foot-last">
                <a href="#" id="toTheTop">
                    <span class="icon-chevron-up"></span>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            &copy; Copyright 2019
                        </div>
                        <div class="span6">
                            <div class="pull-right">Municipalidad Distrital<a href="http://www.proteusthemes.com"> Anco La Mar VRAEM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /bottom footer -->
        </footer> <!-- /footer -->


        <!--  ==========  -->
        <!--  = Modal Windows =  -->
        <!--  ==========  -->




        <!--  ==========  -->
        <!--  = JavaScript =  -->
        <!--  ==========  -->

        <!--  = FB =  -->

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>


        <!--  = jQuery - CDN with local fallback =  -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


        <!--  = _ =  -->

        <!--  = Google Maps API =  -->
        <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyDvMjN1g49P1MA2Onsj-GulDkmDuuH6aoU&amp;sensor=false"></script>

        <script src="../js/Validador.js" type="text/javascript"></script>
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>

        <script src="../js/Ajax.js" type="text/javascript"></script>       
        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>       
        <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>              
        <script src="AdministrarNoticia.js" type="text/javascript"></script>
    </body>
</html>
