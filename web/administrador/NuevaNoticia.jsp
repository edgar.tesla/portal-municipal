


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="gob.muni.bean.BNoticia"%>
<%@page import="gob.muni.bl.BLNoticia"%>
<%String idNoticia = request.getParameter("id");%>
<%
    BLNoticia emp = new BLNoticia(request);
    BNoticia imgvo = null;
    System.out.println("en el jsp actualizar " + (idNoticia == null));
    session.setAttribute("url_fotografia_personal", null);
    session.setAttribute("url_real_fotografia_personal", null);
    session.setAttribute("name_fotografia_personal", null);
    session.setAttribute("ext_fotografia_personal", null);
    if (idNoticia != null && idNoticia != "") {
        imgvo = emp.obtenerNoticia(Integer.parseInt(idNoticia));
        request.getSession().setAttribute("noticia", imgvo);
    } else {
        request.getSession().setAttribute("noticia", null);
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Administracion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content="ProteusThemes"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <!--  Google Fonts  -->
        <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek'
              rel='stylesheet' type='text/css'/>

        <!-- Twitter Bootstrap -->
        <link href="../stylesheets/bootstrap.css" rel="stylesheet"/>
        <link href="../stylesheets/responsive.css" rel="stylesheet"/>

        <!-- Slider Revolution -->
        <link rel="stylesheet" href="../js/rs-plugin/css/settings.css" type="text/css"/>
        <!-- jQuery UI -->
        <link rel="stylesheet" href="../js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="../js/prettyphoto/css/prettyPhoto.css" type="text/css"/>
        <!-- main styles -->
        <link href="../stylesheets/grass-green.css" rel="stylesheet"/>
        <link href="../stylesheets/muni.css" rel="stylesheet"/>
        <link type="text/css" href="../CSS/jquery-ui-1.8.23.custom.css" rel="stylesheet" />



        <!-- Modernizr -->
        <script src="../js/modernizr.custom.56918.js"></script>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" href="../images/muni_anco.jpg"/>
        <link rel="shortcut icon" href="../images/muni_anco.jpg"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    </head>


    <body onload="crearCargador()">


        <!--  ==========  -->
        <!--  = Header =  -->
        <!--  ==========  -->
        <header id="header">
            <div class="container">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Logo =  -->
                    <!--  ==========  -->
                    <div class="span7">
                        <a class="brand" href="index.jsp">
                            <img src="../images/muni_anco.jpg" alt="Webmarket Logo" width="48" height="48"/>
                            <span class="pacifico">Municipalidad</span>
                            <span class="tagline">Distrital Anco La Mar VRAEM 2019 - 2022</span>
                        </a>
                    </div>

                    <!--  ==========  -->
                    <!--  = Social Icons =  -->
                    <!--  ==========  -->
                    <div class="span5">
                        <div class="top-right">

                            <div class="register">
                                <a href="SLogout" >Cerrar Sesion</a>
                            </div>
                        </div>
                    </div> <!-- /social icons -->
                </div>
            </div>
        </header>

        <!--  ==========  -->
        <!--  = Breadcrumbs =  -->
        <!--  ==========  -->
        <div class="darker-stripe">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.jsp">Municipalidad</a>
                            </li>
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="mapa_distrito.jsp">Administracion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  ==========  -->
        <!--  = Main container =  -->
        <!--  ==========  -->
        <div class="container">
            <div class="push-up top-equal blocks-spacer">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Main content =  -->
                    <!--  ==========  -->
                    <section class="span8 single single-post" >
                        <h1><%=(imgvo != null) ? "Editar noticia" : "Crear nueva noticia"%></h1>
                        <form>
                            <input type="hidden" value="<%=(idNoticia == null || idNoticia == "") ? "" : idNoticia%>" name="editar"/>
                            <div>
                                <label>Titulo</label>
                                <input value="<%=(imgvo != null) ? imgvo.getDs_titulo() : ""%>" type="text" name="txt_titulo" maxlength="80" />         
                            </div>
                            <div>
                                <label>Especificacion</label>
                                <textarea value="" type="text" name="txt_especificacion" maxlength="254"><%=(imgvo != null) ? imgvo.getDs_especificacion() : ""%></textarea>         
                            </div>

                            <div>
                                <center>                                                                
                                    <div id="imagen" class="imagen"> 
                                        <%if (idNoticia != null || idNoticia == "") {%>
                                        <img src="SImagenNoticia?idnoticia=<%=idNoticia%>" alt="" id="fotografia_noticia"/>  
                                        <%} else {%>
                                        <img src="../images/logo.png" alt="" id="fotografia_noticia"/>
                                        <%}%>
                                        <br/>
                                        <div align="center" style="width:100px;" id="file_uploader">
                                        </div>                                                                          
                                    </div>
                                </center>
                            </div> 

                            <!--<div>
                                <label>Seleccione Imagen</label>
                                <input value="" type="file" name="im_noticia"/>         
                            </div>-->
                            <input type="button" value="Guardar" onclick="registrarNoticia()"/>
                        </form>
                        <div id="dialog_info" title="Atencion" class="alert">            
                            <div id="dialog_info_msg" class="info">                    
                                <img src="../Recursos/icon/Info.png" width="12" height="12" alt="Alert"/> 
                            </div>               
                        </div>
                        <div id="dialog_alert" title="Error" class="alert">            
                            <div id="dialog_alert_msg" class="alert">
                                <img src="../Recursos/icon/Alert.png" width="12" height="10" alt="Alert"/>  
                            </div>               
                        </div>
                        <div id="dialog_alert_validacion" title="Advertencia" class="alert">            
                            <div id="dialog_alert_validacion_msg" class="alert_validacion">
                                <img src="../Recursos/icon/Warning.png" width="3" height="9" alt="Alert"/>                    
                                Por Favor Llene todos los campos e Ingrese Datos Validos.
                            </div>  
                        </div>
                    </section> <!-- /main content -->

                    <!--  ==========  -->
                    <!--  = Sidebar =  -->
                    <!--  ==========  -->


                </div>
            </div>
        </div> <!-- /container -->

        <!--  ==========  -->
        <!--  = Brands Carousel =  -->
        <!--  ==========  -->



        <!--  ==========  -->
        <!--  = Footer =  -->
        <!--  ==========  -->
        <footer>

            <!--  ==========  -->
            <!--  = Bottom Footer =  -->
            <!--  ==========  -->
            <div class="foot-last">
                <a href="#" id="toTheTop">
                    <span class="icon-chevron-up"></span>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            &copy; Copyright 2019
                        </div>
                        <div class="span6">
                            <div class="pull-right">Municipalidad Distrital<a href="http://www.proteusthemes.com"> Anco La Mar VRAEM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /bottom footer -->
        </footer> <!-- /footer -->

        <!--  = FB =  -->

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>

        <!--  = jQuery - CDN with local fallback =  -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

        <!--  = Google Maps API =  -->
        <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyDvMjN1g49P1MA2Onsj-GulDkmDuuH6aoU&amp;sensor=false"></script>

        <script src="../js/Validador.js" type="text/javascript"></script>
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>

        <script src="../js/Ajax.js" type="text/javascript"></script>       
        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>       
        <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>              
        <script src="AdministrarConvocatoria.js" type="text/javascript"></script>
        <script src="RegistrarNoticia.js" type="text/javascript"></script>

        <link href="../CSS/fileuploader.css" rel="stylesheet" type="text/css" />
        <script src="../js/fileuploader.js" type="text/javascript"></script>
        
    </body>
</html>

