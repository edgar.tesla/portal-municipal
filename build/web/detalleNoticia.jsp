<%-- 
    Document   : mision_vision
    Created on : 22-abr-2019, 16:06:00
    Author     : COMPUTER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="gob.muni.bean.BNoticia"%>
<%@page import="gob.muni.bl.BLNoticia"%>
<%@page import="java.util.ArrayList"%>
<%
    BLNoticia emp = new BLNoticia(request);
    BNoticia imgvo = new BNoticia();
    ArrayList<BNoticia> listar = emp.listarNoticia();

    String pId = request.getParameter("id");
    int id = -1;
    int pos = -1;
    if (pId != null && pId != "") {
        id = Integer.parseInt(pId);
        for (int i = 0; i < listar.size(); i++) {
            if (listar.get(i).getCd_noticia() == id) {
                listar.get(i).getDs_titulo();
                pos = i;
            }
        }
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Detalle noticia</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content="ProteusThemes"/>

        <!--  Google Fonts  -->
        <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek'
              rel='stylesheet' type='text/css'/>

        <!-- Twitter Bootstrap -->
        <link href="stylesheets/bootstrap.css" rel="stylesheet"/>
        <link href="stylesheets/responsive.css" rel="stylesheet"/>
        <!-- Slider Revolution -->
        <link rel="stylesheet" href="js/rs-plugin/css/settings.css" type="text/css"/>
        <!-- jQuery UI -->
        <link rel="stylesheet" href="js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="js/prettyphoto/css/prettyPhoto.css" type="text/css"/>
        <!-- main styles -->

        <link href="stylesheets/grass-green.css" rel="stylesheet"/>


        <!-- Modernizr -->
        <script src="js/modernizr.custom.56918.js"></script>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/muni_anco.jpg"/>
        <link rel="apple-touch-icon-precomposed" href="images/muni_anco.jpg"/>
        <link rel="shortcut icon" href="images/muni_anco.jpg"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>


    <body>


        <!--  ==========  -->
        <!--  = Header =  -->
        <!--  ==========  -->
        <header id="header">
            <div class="container">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Logo =  -->
                    <!--  ==========  -->
                    <div class="span7">
                        <a class="brand" href="index.html">
                            <img src="images/muni_anco.jpg" alt="Webmarket Logo" width="48" height="48"/>
                            <span class="pacifico">Municipalidad</span>
                            <span class="tagline">Distrital Anco La Mar VRAEM 2019 - 2022</span>
                        </a>
                    </div>

                    <!--  ==========  -->
                    <!--  = Social Icons =  -->
                    <!--  ==========  -->
                    <div class="span5">
                        <div class="top-right">
                            <div class="icons">
                                <a href="https://www.facebook.com/munianco.lamar.54"><span class="zocial-facebook"></span></a>
                                <a href="https://twitter.com/proteusnetcom"><span class="zocial-twitter"></span></a>
                            </div>
                            <div class="register">
                                <a href="login.jsp" role="button">Iniciar Sesion</a>
                            </div>
                        </div>
                    </div> <!-- /social icons -->
                </div>
            </div>
        </header>

        <!--  ==========  -->
        <!--  = Main Menu / navbar =  -->
        <!--  ==========  -->
        <div class="navbar navbar-static-top" id="stickyNavbar">
            <div class="navbar-inner">
                <div class="container">
                    <div class="row">
                        <div class="span9">
                            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!--  ==========  -->
                            <!--  = Menu =  -->
                            <!--  ==========  -->
                            <div class="nav-collapse collapse">
                                <ul class="nav" id="mainNavigation">
                                    <li class="active">
                                        <a href="index.jsp" class="dropdown-toggle"> Inicio <b class="caret"></b> </a>
                                    </li>

                                    <li class="dropdown">
                                        <a href="index.jsp" class="dropdown-toggle"> Ciudad <b class="caret"></b> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="mapa_distrito.jsp">Mapa Distrito</a></li>
                                            <li><a href="reseña_historica.jsp">Reseña Historica</a></li>
                                            <li><a href="galeria_turistico.jsp">Galeria Turistico</a></li>
                                            <li><a href="via_acceso.jsp">Via de Acceso</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="index.jsp" class="dropdown-toggle">Municipalidad <b class="caret"></b> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="mision_vision.jsp">Mision Vision</a></li>
                                            <li><a href="organigrama.jsp">Organigrama</a></li>
                                            <li><a href="alcalde_funicionarios.jsp">Alcalde y Funcionarios</a></li>
                                            <li><a href="comision_regidores.jsp">Comision Regidores</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="dropdown">
                                        <a href="index.jsp" class="dropdown-toggle">Transparencia <b class="caret"></b> </a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown">
                                                <a href="features.html" class="dropdown-toggle"><i class="icon-caret-right pull-right visible-desktop"></i>
                                                    Documentos</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="features.html#headings">MOD</a></li>
                                                    <li><a href="features.html#alertBoxes">TUPA</a></li>
                                                    <li><a href="features.html#tabs">ROF</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown">
                                                <a href="features.html" class="dropdown-toggle"><i class="icon-caret-right pull-right visible-desktop"></i>
                                                    Contrataciones</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="features.html#headings">Plan Anual de Contrataciones</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown">
                                                <a href="features.html" class="dropdown-toggle"><i class="icon-caret-right pull-right visible-desktop"></i>
                                                    Ciudadania</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="features.html#headings">Presupuesto Participativo</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="comunicado.jsp">Comunicado</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="convocatoria.jsp">Convocatoria</a></li>
                                </ul>

                                <!--  ==========  -->
                                <!--  = Search form =  -->
                                <!--  ==========  -->
                                <!--form class="navbar-form pull-right" action="#" method="get" />
                                    <button type="submit"><span class="icon-search"></span></button>
                                    <input type="text" class="span1" name="search" id="navSearchInput" />
                                </form-->
                            </div><!-- /.nav-collapse -->
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /main menu -->


        <!--  ==========  -->
        <!--  = Slider Revolution =  -->
        <!--  ==========  -->
        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner">
                <ul>
                    <li data-transition="premium-random" data-slotamount="3">
                        <img src="images/dummy/slides/1/slide.jpg" alt="slider img" width="1400" height="377"/>

                        <!-- baloons -->

                        <a href="reseña_historica.jsp" class="caption lfl btn btn-primary btn_theme" data-x="120" data-y="260"
                           data-speed="1000" data-start="900" data-easing="easeInOutBack">
                            Reseña Historica
                        </a>
                    </li><!-- /slide -->

                    <li data-transition="premium-random" data-slotamount="3">
                        <img src="images/dummy/slides/2/slide.jpg" alt="slider img" width="1400" height="377"/>
                        <!-- plane -->
                        <div class="caption lfl str" data-x="400" data-y="20" data-speed="10000" data-start="1000"
                             data-easing="linear">
                        </div>

                        <!-- texts -->

                        <a href="galeria_turistico.jsp" class="caption lfl btn btn-primary btn_theme" data-x="120" data-y="260"
                           data-speed="1000" data-start="900" data-easing="easeInOutBack">
                            Galeria Turistico
                        </a>
                    </li><!-- /slide -->
                    <li data-transition="premium-random" data-slotamount="3">
                        <img src="images/dummy/slides/3/slide.jpg" alt="slider img" width="1400" height="377"/>

                        <a href="https://www.ceplan.gob.pe/documentos_/plan-bicentenario-el-peru-hacia-el-2021/" class="caption lfl btn btn-primary btn_theme" data-x="120" data-y="260"
                           data-speed="1000" data-start="900" data-easing="easeInOutBack">
                            Plan Bicentenario: el Perú hacia el 2021
                        </a>
                    </li><!-- /slide -->

                    <li data-transition="premium-random" data-slotamount="3">
                        <img src="images/dummy/slides/4/slide.jpg" alt="slider img" width="1400" height="377"/>

                        <!-- texts -->

                        <div class="caption lfl small_theme" data-x="120" data-y="210" data-speed="1000" data-start="700"
                             data-easing="easeInOutBack">
                            Trabajando para una buena gestion 
                            <a href="alcalde_funicionarios" target="_blank"> Municipal</a>!
                        </div>

                        <a href="alcalde_funicionarios.jsp" class="caption lfl btn btn-primary btn_theme" data-x="120"
                           data-y="280" data-speed="1000" data-start="900" data-easing="easeInOutBack">
                            Alcalde y Funcionarios
                        </a>
                    </li><!-- /slide -->
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
            <!--  ==========  -->
            <!--  = Nav Arrows =  -->
            <!--  ==========  -->
            <div id="sliderRevLeft"><i class="icon-chevron-left"></i></div>
            <div id="sliderRevRight"><i class="icon-chevron-right"></i></div>
        </div> <!-- /slider revolution -->

        <!--  ==========  -->
        <!--  = Breadcrumbs =  -->
        <!--  ==========  -->
        <div class="darker-stripe">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.jsp">Municipalidad</a>
                            </li>
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="index.jsp">Ciudad</a>
                            </li>
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="mision_vision.jsp">Reseña Historica</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  ==========  -->
        <!--  = Main container =  -->
        <!--  ==========  -->
        <div class="container">
            <div class="push-up top-equal blocks-spacer">
                <div class="row">

                    <!--  ==========  -->
                    <!--  = Main content =  -->
                    <!--  ==========  -->
                    <section class="span8 single single-post">
                        <%if (pos != -1) {%>
                        <p style="text-transform: uppercase; text-align: center">
                            <%=listar.get(pos).getDs_titulo()%>
                        </p>
                        <div class="underlined push-down-20">
                            <img src="SImagenNoticia?idnoticia=<%=listar.get(pos).getCd_noticia()%>"/>
                        </div> <!-- /title -->
                        <p><%=listar.get(pos).getDs_especificacion()%></p>
                        <%} else {%>
                        <h2>No hay informacion de la noticia</h2>
                        <%}%>
                    </section> <!-- /main content -->

                    <!--  ==========  -->
                    <!--  = Sidebar =  -->
                    <!--  ==========  -->
                    <section class="span4 right-sidebar">
                        <h1>Noticias relacionadas</h1>
                        <%
                            for (int j = 0; j < listar.size(); j++) {
                                if (pos != -1 && pos != j) {
                        %>
                        <div>
                            <img src="SImagenNoticia?idnoticia=<%=listar.get(j).getCd_noticia()%>"/>
                            <p><%=listar.get(j).getDs_titulo()%></p>
                            <span><a href="detalleNoticia.jsp?id=<%=listar.get(j).getCd_noticia()%>">Ver detalle</a></span>
                        </div>
                        <%}
                            }%>

                    </section> <!-- /sidebar --> 

                </div>
            </div>
        </div> <!-- /container -->

        <!--  ==========  -->
        <!--  = Brands Carousel =  -->
        <!--  ==========  -->
        <div class="container blocks-spacer-last">

            <!--  ==========  -->
            <!--  = Title =  -->
            <!--  ==========  -->
            <div class="row">
                <div class="span12">
                    <div class="main-titles lined">
                        <h2 class="title"><span class="light">Paginas</span> Principales</h2>
                        <div class="arrows">
                            <a href="#" class="icon-chevron-left" id="brandsLeft"></a>
                            <a href="#" class="icon-chevron-right" id="brandsRight"></a>
                        </div>
                    </div>
                </div>
            </div> <!-- /title -->

            <!--  ==========  -->
            <!--  = Logos =  -->
            <!--  ==========  -->
            <div class="row">
                <div class="span12">
                    <div class="brands carouFredSel" data-nav="brands" data-autoplay="true">
                        <img id="1" src="images/dummy/brands/brands_01.jpg" alt="" width="200" height="104" onclick="mostrarOpcion(this.id)"/>
                        <a target="_blank" href="https://diariocorreo.pe/"><img src="images/dummy/brands/brands_02.jpg" alt="" width="200" height="104"/></a>
                        <a href="https://www.regionayacucho.gob.pe/"><img src="images/dummy/brands/brands_03.jpg" alt="" width="200" height="104" /></a>
                        <img src="images/dummy/brands/brands_04.jpg" alt="" width="200" height="104" onclick="http://www.hospitalregionalayacucho.gob.pe/"/>
                        <img src="images/dummy/brands/brands_05.jpg" alt="" width="200" height="104" onclick="https://rpp.pe/"/>
                        <img src="images/dummy/brands/brands_06.jpg" alt="" width="200" height="104" onclick="http://www.sunat.gob.pe/"/>
                        <img src="images/dummy/brands/brands_07.jpg" alt="" width="200" height="104" onclick="http://www.unsch.edu.pe/"/>
                        <img src="images/dummy/brands/brands_08.jpg" alt="" width="200" height="104" onclick="https://www.mef.gob.pe/es/"/>
                        <img src="images/dummy/brands/brands_04.jpg" alt="" width="200" height="104" onclick="http://www.hospitalregionalayacucho.gob.pe/"/>
                        <img src="images/dummy/brands/brands_03.jpg" alt="" width="200" height="104" onclick="https://www.regionayacucho.gob.pe/"/>
                        <img src="images/dummy/brands/brands_08.jpg" alt="" width="200" height="104" onclick="https://www.mef.gob.pe/es/"/>
                        <img src="images/dummy/brands/brands_02.jpg" alt="" width="200" height="104" onclick="https://diariocorreo.pe/"/>
                    </div>
                </div>
            </div> <!-- /logos -->
        </div> <!-- /brands carousel -->


        <!--  ==========  -->
        <!--  = Footer =  -->
        <!--  ==========  -->
        <footer>

            <!--  ==========  -->
            <!--  = Bottom Footer =  -->
            <!--  ==========  -->
            <div class="foot-last">
                <a href="#" id="toTheTop">
                    <span class="icon-chevron-up"></span>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            &copy; Copyright 2019
                        </div>
                        <div class="span6">
                            <div class="pull-right">Municipalidad Distrital<a href="http://www.proteusthemes.com"> Anco La Mar VRAEM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /bottom footer -->
        </footer> <!-- /footer -->


        <!--  ==========  -->
        <!--  = Modal Windows =  -->
        <!--  ==========  -->

        <!--  = Login =  -->
        <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
             aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="loginModalLabel"><span class="light">Login</span> To Webmarket</h3>
            </div>
            <div class="modal-body">
                <form method="post" action="#"/>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputEmail">Username</label>
                    <div class="controls">
                        <input type="text" class="input-block-level" id="inputEmail" placeholder="Username"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputPassword">Password</label>
                    <div class="controls">
                        <input type="password" class="input-block-level" id="inputPassword" placeholder="Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox"/>
                            Remember me
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary input-block-level bold higher">
                    SIGN IN
                </button>
                </form>
                <p class="center-align push-down-0">
                    <a href="#" data-dismiss="modal">Forgot your password?</a>
                </p>
            </div>
        </div>

        <!--  = Register =  -->
        <div id="registerModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel"
             aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="registerModalLabel"><span class="light">Register</span> To Webmarket</h3>
            </div>
            <div class="modal-body">
                <form method="post" action="#"/>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputUsernameRegister">Username</label>
                    <div class="controls">
                        <input type="text" class="input-block-level" id="inputUsernameRegister" placeholder="Username"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputEmailRegister">Email</label>
                    <div class="controls">
                        <input type="email" class="input-block-level" id="inputEmailRegister" placeholder="Email"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label hidden shown-ie8" for="inputPasswordRegister">Password</label>
                    <div class="controls">
                        <input type="password" class="input-block-level" id="inputPasswordRegister" placeholder="Password"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger input-block-level bold higher">
                    REGISTER
                </button>
                </form>
                <p class="center-align push-down-0">
                    <a data-toggle="modal" role="button" href="#loginModal" data-dismiss="modal">Already Registered?</a>
                </p>

            </div>
        </div>


        <!--  ==========  -->
        <!--  = JavaScript =  -->
        <!--  ==========  -->

        <!--  = FB =  -->

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>


        <!--  = jQuery - CDN with local fallback =  -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
                            if (typeof jQuery == 'undefined') {
                            document.write('<script src="js/jquery.min.js"><\/script>');
                            }
        </script>

        <!--  = _ =  -->
        <script src="js/underscore/underscore-min.js" type="text/javascript"></script>

        <!--  = Bootstrap =  -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

        <!--  = Slider Revolution =  -->
        <script src="js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
        <script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>

        <!--  = CarouFredSel =  -->
        <script src="js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>

        <!--  = jQuery UI =  -->
        <script src="js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.10.3/touch-fix.min.js" type="text/javascript"></script>

        <!--  = Isotope =  -->
        <script src="js/isotope/jquery.isotope.min.js" type="text/javascript"></script>

        <!--  = Tour =  -->
        <script src="js/bootstrap-tour/build/js/bootstrap-tour.min.js" type="text/javascript"></script>

        <!--  = PrettyPhoto =  -->
        <script src="js/prettyphoto/js/jquery.prettyPhoto.js" type="text/javascript"></script>

        <!--  = Google Maps API =  -->
        <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyDvMjN1g49P1MA2Onsj-GulDkmDuuH6aoU&amp;sensor=false"></script>
        <script type="text/javascript" src="js/goMap/js/jquery.gomap-1.3.2.min.js"></script>

        <!--  = Custom JS =  -->
        <script src="js/custom.js" type="text/javascript"></script>

    </body>
</html>
