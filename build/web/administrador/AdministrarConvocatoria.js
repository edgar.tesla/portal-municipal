var request = Ajax.getXMLHTTPRequest();
var id_convocatoria=null
$(function () {
    $('#dialog_info').dialog({
        modal: true,
        autoOpen: false,
        width: 300,
        buttons: {
            "Aceptar": function (event, ui) {
                if(id_convocatoria!=null){
                    eliminarConvocatoria(id_convocatoria); 
                }
            },
            "Cancelar":function(){
                $(this).dialog("close");
            }
        },
        close: function( event, ui ) {            
        }
    });   
});
function listaConvocatoria() {
    var ahora = new Date();
    var id = Math.abs(Math.sin(ahora.getTime()));
    var servlet;
    var servlet = "../SMantenerConvocatoria?op=listar_convocatoria_admin&id=" + id;
    request.open("GET", servlet, true);

    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                var response = request.responseText;

                var listaConvocatoria = document.getElementById("listaConvocatoria");
                listaConvocatoria.innerHTML = response;
            }
        } catch (err) {
            alert(err);
        }
    }
    request.send(null);
}

function obtenerConvocatoria(id_convocatoria) {
    var ahora = new Date();
    var id = Math.abs(Math.sin(ahora.getTime()));
    var servlet;
    servlet = "../SMantenerConvocatoria?op=recuperar_datos_convocatoria&id=" + id;
    servlet += "&id_convocatoria=" + id_convocatoria;
    request.open("GET", servlet, true);
    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                
                mostrarEditar();
                ocultarListar();
                var response = request.responseText;
                var datos_convocatoria = eval(response);
                document.getElementsByName("txt_idconvocatoria").item(0).value = datos_convocatoria.id_convocatoria;
                document.getElementsByName("txt_nombre").item(0).value = datos_convocatoria.ds_nombre;
                document.getElementsByName("txt_based").item(0).value = datos_convocatoria.ds_based;
                document.getElementsByName("txt_basel").item(0).value = datos_convocatoria.ds_basel;
                document.getElementsByName("txt_evaluaciond").item(0).value = datos_convocatoria.ds_evaluaciond;
                document.getElementsByName("txt_evaluacionl").item(0).value = datos_convocatoria.ds_evaluacionl;
                document.getElementsByName("txt_entrevistad").item(0).value = datos_convocatoria.ds_entrevistad;
                document.getElementsByName("txt_entrevistal").item(0).value = datos_convocatoria.ds_entrevistal;
                document.getElementsByName("txt_resultadod").item(0).value = datos_convocatoria.ds_resultadod;
                document.getElementsByName("txt_resultadol").item(0).value = datos_convocatoria.ds_resultadol;
            }
        } catch (err) {
            alert(err);
        }
    }
    request.send(null);
}

function actualizarConvocatoria() {
    var txt_idconvocatoria = document.getElementsByName("txt_idconvocatoria").item(0);
    var txt_nombre = document.getElementsByName("txt_nombre").item(0);
    var txt_based = document.getElementsByName("txt_based").item(0);
    var txt_basel = document.getElementsByName("txt_basel").item(0);
    var txt_evaluaciond = document.getElementsByName("txt_evaluaciond").item(0);
    var txt_evaluacionl = document.getElementsByName("txt_evaluacionl").item(0);
    var txt_entrevistad = document.getElementsByName("txt_entrevistad").item(0);
    var txt_entrevistal = document.getElementsByName("txt_entrevistal").item(0);
    var txt_resultadod = document.getElementsByName("txt_resultadod").item(0);
    var txt_resultadol = document.getElementsByName("txt_resultadol").item(0);
    if (txt_nombre.value.length <= 0
            | txt_based.value.length <= 0
            | txt_basel.value.length <= 0
            ) {
        alert('los tres primeros campos son obligatorios');
    } else {
        var ahora = new Date();
        var id = Math.abs(Math.sin(ahora.getTime()));
        var servlet;
        servlet = "../SMantenerConvocatoria?op=modificar_datos_convocatoria&id=" + id;
        servlet += "&txt_idconvocatoria=" + txt_idconvocatoria.value;
        servlet += "&txt_nombre=" + txt_nombre.value;
        servlet += "&txt_based=" + txt_based.value;
        servlet += "&txt_basel=" + txt_basel.value;
        servlet += "&txt_evaluaciond=" + txt_evaluaciond.value;
        servlet += "&txt_evaluacionl=" + txt_evaluacionl.value;
        servlet += "&txt_entrevistad=" + txt_entrevistad.value;
        servlet += "&txt_entrevistal=" + txt_entrevistal.value;
        servlet += "&txt_resultadod=" + txt_resultadod.value;
        servlet += "&txt_resultadol=" + txt_resultadol.value;
        request.open("GET", servlet, true);
        request.onreadystatechange = function () {
            try {
                if (request.readyState == 4) {
                    alert('hola sistemas');
                    var response = request.responseText;
                    var response_event = eval(response);
                    var respt = response_event.respt;
                    var msg = response_event.msg;
                    alert('como es !!!!');
                    if (respt.indexOf("exito") >= 0) {
                        //resetComponent();
                        alert(msg);
                        location.href = "administrar_Convocatoria.jsp";
                    } else if (respt.indexOf("error") >= 0) {
                        alert(msg);
                        location.href = "administrar_Convocatoria.jsp";
                    }
                }
            } catch (err) {
                alert('Error al registrar');
            }
        }
        request.send(null);
    }
}

function eliminarConvocatoria(id) {
    var servlet;
    servlet = "/SMantenerConvocatoria?op=eliminar_convocatoria&id_convocatoria=" + id;
    request.open("GET", servlet, true);
    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                var response = request.responseText;
                var response_event = eval(response);
                var respt = response_event.respt;
                var msg = response_event.msg;
                if (respt.indexOf("exito") >= 0) {
                    //resetComponent();
                    location.href = "administrar_Convocatoria.jsp";
                } else if (respt.indexOf("error") >= 0) {
                    location.href = "administrar_Convocatoria.jsp";
                }
            }
        } catch (err) {
            alert('Error al registrar');
        }
    }
    request.send(null);

}
function mostrarDialogConvocatoria(id){
    id_convocatoria=id;
    $('#dialog_info_msg').append("¿Esta seguro de eliminar la convocatoria?");
    $('#dialog_info').dialog("open");
}

function mostrarEditar(){
    var eMostrar = document.getElementById("seccionEditConvocatoria");
    eMostrar.style.display="block";
}
function ocultarEditar(){
    var eMostrar = document.getElementById("seccionEditConvocatoria");
    eMostrar.style.display="none";
}
function ocultarListar(){
    var eMostrar = document.getElementById("seccionLista");
    eMostrar.style.display="none";
}
function mostrarListar(){
    var eMostrar = document.getElementById("seccionLista");
    eMostrar.style.display="none";
}

