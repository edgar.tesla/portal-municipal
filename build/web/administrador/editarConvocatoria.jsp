<%-- 
    Document   : editarConvocatoria
    Created on : 25-abr-2019, 10:41:58
    Author     : COMPUTER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Twitter Bootstrap -->
        <link href="../stylesheets/bootstrap.css" rel="stylesheet"/>
        <link href="../stylesheets/responsive.css" rel="stylesheet"/>
        <link href="../stylesheets/grass-green.css" rel="stylesheet"/>
        <script src="../js/Validador.js" type="text/javascript"></script>
        <!--  = Bootstrap =  -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>

        <script src="../js/Ajax.js" type="text/javascript"></script>       
        <link type="text/css" href="../CSS/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>       
        <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>              
        <script src="AdministrarConvocatoria.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <section class="span8 single single-post">
                    <fieldset>
                        <legend>Denominacion</legend>
                        <label>Convocatoria</label>
                        <input value="" type="text" name="txt_nombre" maxlength="30"/>
                    </fieldset>
                    <fieldset>
                        <legend>Bases</legend>
                        <div>
                            <label>Descripcion</label>
                            <input value="" type="text" name="txt_based" max="15"/>
                        </div>
                        <div>
                            <label>Link</label>
                            <input class="form-control input-xxlarge" value="" type="text" name="txt_basel"/>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Evaluacion</legend>
                        <div>
                            <label>Descripcion</label>
                            <input value="" type="text" name="txt_evaluaciond" maxlength="15"/>
                        </div>
                        <div>
                            <label>Link</label>
                            <input class="form-control input-xxlarge" value="" type="text" name="txt_evaluacionl"/>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Entrevista</legend>
                        <div>
                            <label>Descripcion</label>
                            <input value="" type="text" name="txt_entrevistad" maxlength="15"/>
                        </div>
                        <div>
                            <label>Link</label>
                            <input class="form-control input-xxlarge" value="" type="text" name="txt_entrevistal"/>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Resultado</legend>
                        <div>
                            <label>Descripcion</label>
                            <input value="" type="text" name="txt_resultadod" maxlength="15"/>
                        </div>
                        <div>
                            <label>Link</label>
                            <input class="form-control input-xxlarge" value="" type="text" name="txt_resultadol"/>
                        </div>
                    </fieldset>
                </section>
            </div> 
            <input class="btn btn-default" value="Registrar" type="button" onclick="registrarConvocatoria();"/>
            <a class="btn btn-default" href="administrar_Convocatoria.jsp">Regresar</a>
        </div>
    </body>
</html>
