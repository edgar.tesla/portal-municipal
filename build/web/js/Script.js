/*=============== menu lib ===============*/
stuHover = function() {
	var cssRule;
	var newSelector;
	for (var i = 0; i < document.styleSheets.length; i++)
		for (var x = 0; x < document.styleSheets[i].rules.length ; x++)
			{
			cssRule = document.styleSheets[i].rules[x];
			if (cssRule.selectorText.indexOf("LI:hover") != -1)
			{
				 newSelector = cssRule.selectorText.replace(/LI:hover/gi, "LI.iehover");
				document.styleSheets[i].addRule(newSelector , cssRule.style.cssText);
			}
		}
	var getElm = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<getElm.length; i++) {
		getElm[i].onmouseover=function() {
			this.className+=" iehover";
		}
		getElm[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", stuHover);
/*=============== fin menu lib ===============*/
 function showModalBox(titulo,url,ancho,largo){     
     this.blur(); 
     Modalbox.show(url, {title: titulo, width: ancho, height: largo}); 
     return false;     
 }
/*=============== Util ModalBox lib ===============*/
/*=============== fin Util ModalBox lib ===============*/

/*=============== Util Nieve lib ===============*/
$(document).ready(function(){
			$(document).snowfall();
		});
/*=============== fin Util Nieve lib ===============*/

/*=============== Util Banner lib ===============*/
$(document).ready(
	function() {
		var $container = $(".container");
		
		$container.wtRotator({
			width:1200,
			height:150,
			thumb_width:24,
			thumb_height:24,
			button_width:24,
			button_height:24,
			button_margin:5,
			auto_start:true,
			delay:6000,
			transition:"random",
			transition_speed:800,
			easing:"easeInOutElastic",
			block_size:75,
			vert_size:55,
			horz_size:50,
			cpanel_align:"BR",
			timer_align:"top",
			display_thumbs:false,
			display_dbuttons:false,
			display_playbutton:false,
			display_thumbimg:false,
			display_side_buttons:false,
			tooltip_type:"image",
			display_numbers:false,
			display_timer:true,
			mouseover_select:false,
			mouseover_pause:false,
			cpanel_mouseover:false,
			text_mouseover:false,
			text_effect:"fade",
			text_sync:true,
			shuffle:true,
			block_delay:25,
			vstripe_delay:73,
			hstripe_delay:183
		});
		
	}
);
/*=============== fin Util Banner lib ===============*/

/*=============== utilitarios lib ===============*/
var hora_12 = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

function simularFecha_Hora_Actual(id_ref) {
    dateObject = new Date();
    var utc_hrs = dateObject.getUTCHours();
    var tiempo = "";
    if (utc_hrs >= 0 && utc_hrs <= 12) {
        tiempo = "AM";
    } else if (utc_hrs >= 13 && utc_hrs <= 23) {
        tiempo = "PM";
    }
    var msg_page_footer = document.getElementById(id_ref);
    /*msg_page_footer.textContent = "ayacucho - " + dateObject.getDate()
    + "/" + dateObject.getMonth()
    + "/" + dateObject.getFullYear()*/
    msg_page_footer.textContent = ""
    + hora_12[dateObject.getHours()]
    + ":" + dateObject.getMinutes()
    + ":" + dateObject.getSeconds()
    + " " + tiempo;

    setTimeout('simularFecha_Hora_Actual('+id_ref+')', 1000);
}
function simularFecha_Hora_Actual_Corto(id_ref) {
    dateObject = new Date();
    var utc_hrs = dateObject.getUTCHours();
    var tiempo = "";
    if (utc_hrs >= 0 && utc_hrs <= 12) {
        tiempo = "AM";
    } else if (utc_hrs >= 13 && utc_hrs <= 23) {
        tiempo = "PM";
    }
    var txt = document.getElementById(id_ref);
    /*msg_page_footer.textContent = "ayacucho - " + dateObject.getDate()
    + "/" + dateObject.getMonth()
    + "/" + dateObject.getFullYear()*/
    txt.value = ""
    + hora_12[dateObject.getHours()]
    + ":" + dateObject.getMinutes()
    + " " + tiempo;

    setTimeout('simularFecha_Hora_Actual_Corto('+id_ref+')', 1000);
}

function initControlPage() {
    simularFecha_Hora_Actual("id_control");
    validaSeleccionItemCorrectoUsuario();   
}
function salir(){
    window.close();
}
/*=============== fin utilitarios lib ===============*/

/*=============== Grid lib ===============*/

/*=============== fin Grid lib ===============*/

/*=============== builder lib ===============*/
/*=============== fin builder lib ===============*/

/*=============== effects lib ===============*/
/*=============== fin effects lib ===============*/

/*=============== prototype lib ===============*/
/*=============== fin prototype lib ===============*/

/*=============== scriptaculous lib ===============*/
/*=============== fin scriptaculous lib ===============*/

