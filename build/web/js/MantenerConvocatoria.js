var request = Ajax.getXMLHTTPRequest();
function listarConvocatoria() {
    var ahora = new Date();
    var id = Math.abs(Math.sin(ahora.getTime()));

    var servlet;
    var servlet = "SMantenerConvocatoria?op=listar_convocatoria&id=" + id;
    request.open("GET", servlet, true);

    request.onreadystatechange = function () {
        try {
            if (request.readyState == 4) {
                var response = request.responseText;

                var listaConvocatoria = document.getElementById("listaConvocatoria");
                listaConvocatoria.innerHTML = response;
            }
        } catch (err) {
            alert(err);
        }
    }
    request.send(null);
}

function cargarDatosInsumo(id_insumo) {
    var check = document.getElementById(id_insumo);
    var ahora = new Date();
    var id = Math.abs(Math.sin(ahora.getTime()));
    var servlet;
    if (check.checked) {
        servlet = "../SMantenerProveedor?op=recuperar_datos_insumo&id=" + id;
        servlet += "&id_insumo=" + id_insumo;
        request.open("GET", servlet, true);
        request.onreadystatechange = function () {
            try {
                if (request.readyState == 4) {
                    var response = request.responseText;
                    var datos_proveedor = eval(response);
                    document.getElementsByName("txt_id_proveedor").item(0).value = datos_proveedor.id_proveedor;
                    document.getElementsByName("txt_ruc").item(0).value = datos_proveedor.ruc;
                }
            } catch (err) {
                alert(err);
            }
        }
        request.send(null);
    } else {
        resetComponent();
    }
}



function actualizarProveedor() {
    var txt_id_proveedor = document.getElementsByName("txt_id_proveedor").item(0);
    var txt_ruc = document.getElementsByName("txt_ruc").item(0);
    var txt_razon_social = document.getElementsByName("txt_razon_social").item(0);
    var txt_representante = document.getElementsByName("txt_representante").item(0);
    var txt_direccion = document.getElementsByName("txt_direccion").item(0);
    var txt_ciudad = document.getElementsByName("txt_ciudad").item(0);
    var txt_telefono = document.getElementsByName("txt_telefono").item(0);
    var txt_fax = document.getElementsByName("txt_fax").item(0);
    var txt_correo_electronico = document.getElementsByName("txt_correo_electronico").item(0);
    var txt_descripcion = document.getElementsByName("txt_descripcion").item(0);
    if (txt_id_proveedor.value.length <= 0
            | txt_ruc.value.length <= 0
            | txt_razon_social.value.length <= 0
            | txt_representante.value.length <= 0
            | txt_direccion.value.length <= 0
            | txt_ciudad.value.length <= 0
            | txt_telefono.value.length <= 0
            | txt_fax.value.length <= 0
            | txt_correo_electronico.value.length <= 0
            | txt_descripcion.value.length <= 0) {
        $('#dialog_alert_validacion').dialog('open');
    } else {
        var ahora = new Date();
        var id = Math.abs(Math.sin(ahora.getTime()));
        var servlet;
        servlet = "../SMantenerProveedor?op=actualizar_proveedor&id=" + id;
        servlet += "&txt_id_proveedor=" + txt_id_proveedor.value;
        servlet += "&txt_ruc=" + txt_ruc.value;
        servlet += "&txt_razon_social=" + txt_razon_social.value;
        servlet += "&txt_representante=" + txt_representante.value;
        servlet += "&txt_direccion=" + txt_direccion.value;
        servlet += "&txt_ciudad=" + txt_ciudad.value;
        servlet += "&txt_telefono=" + txt_telefono.value;
        servlet += "&txt_fax=" + txt_fax.value;
        servlet += "&txt_correo_electronico=" + txt_correo_electronico.value;
        servlet += "&txt_descripcion=" + txt_descripcion.value;
        request.open("GET", servlet, true);
        request.onreadystatechange = function () {
            try {
                if (request.readyState == 4) {
                    var response = request.responseText;
                    var response_event = eval(response);
                    var respt = response_event.respt;
                    var msg = response_event.msg;
                    //alert(respt+"-"+msg);
                    if (respt.indexOf("exito") >= 0) {
                        resetComponent();
                        listar_proveedores("txt_referencia");
                        $('#dialog_info_msg').append(msg);
                        $('#dialog_info').dialog('open');
                    } else if (respt.indexOf("error") >= 0) {
                        $('#dialog_alert_msg').append(msg);
                        $('#dialog_alert').dialog('open');
                    }
                }
            } catch (err) {
                alert(err);
            }
        }
        request.send(null);
    }
}
function resetComponent() {
    document.getElementsByName("txt_id_proveedor").item(0).value = "";
    document.getElementsByName("txt_ruc").item(0).value = "";
    document.getElementsByName("txt_razon_social").item(0).value = "";
    document.getElementsByName("txt_representante").item(0).value = "";
    document.getElementsByName("txt_direccion").item(0).value = "";
    document.getElementsByName("txt_ciudad").item(0).value = "";
    document.getElementsByName("txt_telefono").item(0).value = "";
    document.getElementsByName("txt_fax").item(0).value = "";
    document.getElementsByName("txt_correo_electronico").item(0).value = "";
    document.getElementsByName("txt_descripcion").item(0).value = "";
}

