var request = Ajax.getXMLHTTPRequest();

function validarLogueoUsuario() {
    var txt_usuario = document.getElementsByName("txt_usuario").item(0);
    var txt_clave = document.getElementsByName("txt_clave").item(0);
    if (txt_usuario.value.length <= 0
            | txt_clave.value.length <= 0) {
        alert('Ingrese Todos los campos');
    } else {
        var ahora = new Date();
        var id = Math.abs(Math.sin(ahora.getTime()));
        var servlet;
        servlet = "SIniciarSession?op=validar_logueo&id=" + id;
        servlet += "&txt_usuario=" + txt_usuario.value;
        servlet += "&txt_clave=" + txt_clave.value;
        request.open("GET", servlet, true);
        request.onreadystatechange = function () {
            try {
                if (request.readyState == 4) {
                    var response = request.responseText;
                    var response_event = eval(response);
                    var respt = response_event.respt;
                    var pagina = response_event.pagina;
                    var msg = response_event.msg;
                    if (respt.indexOf("exito") >= 0) {
                        alert(msg);
                        resetComponent();
                        location.href = pagina;
                    } else if (respt.indexOf("error") >= 0) {
                        alert(msg);
                    }
                }
            } catch (err) {
                alert(err);
            }
        }
        request.send(null);
    }
}
function resetComponent() {
    document.getElementsByName("txt_usuario").item(0).value = "";
    document.getElementsByName("txt_clave").item(0).value = "";
}


