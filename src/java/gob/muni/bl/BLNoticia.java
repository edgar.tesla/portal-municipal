/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bl;

import gob.muni.bean.BNoticia;
import gob.muni.dao.DAONoticia;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.rx.cr.ds.DSConeccion;

/**
 *
 * @author COMPUTER
 */
public class BLNoticia {

    private DAONoticia dao;
    private DSConeccion ds;
    private HttpServletRequest request;

    public BLNoticia(DSConeccion ds) {
        this.request = null;
        this.ds = ds;
    }

    public BLNoticia(HttpServletRequest request) {
        this.request = request;
    }

    public void guardarNoticia(BNoticia bean) {
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            dao.registrar(bean);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editarNoticia(BNoticia bean) {
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            dao.actualizar(bean);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<BNoticia> listarNoticia() {
        ArrayList<BNoticia> lista;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            lista = dao.listar();
            dao.conectionClose();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public InputStream obtenerImagenNoticia(int idNoticia) {
        InputStream is = null;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            is = dao.obtenerImagenNoticia(idNoticia);
            dao.conectionClose();

            return is;
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
            return is;
        }
    }

    public BNoticia obtenerNoticia(int idNoticia) {
        BNoticia is = null;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            is = dao.buscar(idNoticia);
            dao.conectionClose();

            return is;
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
            return is;
        }
    }

    public void eliminarNoticia(BNoticia bNoticia) {
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAONoticia(ds.getConeccion());
            dao.eliminar(bNoticia);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
