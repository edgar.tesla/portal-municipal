/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bl;

import gob.muni.bean.BUsuario;
import gob.muni.dao.DAOUsuario;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.rx.cr.ds.DSConeccion;

/**
 *
 * @author COMPUTER
 */
public class BLUsuario {

    private DAOUsuario dao;
    private DSConeccion ds;
    private HttpServletRequest request;

    public BLUsuario(DSConeccion ds) {
        this.request = null;
        this.ds = ds;
    }

    public BLUsuario(HttpServletRequest request) {
        this.request = request;
    }

    public BUsuario validadLogueUsuario(String usuario, String clave) {
        BUsuario bean;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOUsuario(ds.getConeccion());
            bean = dao.validarLogueoUsuario(usuario, clave);
            dao.conectionClose();
            return bean;
        } catch (SQLException ex) {
            Logger.getLogger(BLUsuario.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
