/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bl;

import gob.muni.bean.BConvocatoria;
import gob.muni.bean.BNoticia;
import gob.muni.dao.DAOConvocatoria;
import gob.muni.dao.DAONoticia;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.rx.cr.ds.DSConeccion;

/**
 *
 * @author COMPUTER
 */
public class BLConvocatoria {

    private DAOConvocatoria dao;
    private DSConeccion ds;
    private HttpServletRequest request;

    public BLConvocatoria(DSConeccion ds) {
        this.request = null;
        this.ds = ds;
    }

    public BLConvocatoria(HttpServletRequest request) {
        this.request = request;
    }

    public void guardarConvocatoria(BConvocatoria bean) {
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOConvocatoria(ds.getConeccion());
            dao.registrar(bean);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLConvocatoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void actualizarConvocatoria(BConvocatoria bean) {
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOConvocatoria(ds.getConeccion());
            dao.actualizar(bean);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLConvocatoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<BConvocatoria> listarConvocatoria() {
        ArrayList<BConvocatoria> lista;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOConvocatoria(ds.getConeccion());
            lista = dao.listar();
            dao.conectionClose();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(BLConvocatoria.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public BConvocatoria buscarConvocatoria(int id) {
        BConvocatoria bean;
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOConvocatoria(ds.getConeccion());
            bean = dao.buscar(id);
            dao.conectionClose();
            return bean;
        } catch (SQLException ex) {
            Logger.getLogger(BLConvocatoria.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public void eliminarConvocatoria(BConvocatoria bConvocatoria){
        try {
            if (request != null && ds == null) {
                ds = new DSConeccion(request.getServletContext().getInitParameter("host"), request.getServletContext().getInitParameter("port"), request.getServletContext().getInitParameter("db"), request.getServletContext().getInitParameter("user"), request.getServletContext().getInitParameter("password"));
            }
            dao = new DAOConvocatoria(ds.getConeccion());
            dao.eliminar(bConvocatoria);
            dao.conectionClose();
        } catch (SQLException ex) {
            Logger.getLogger(BLConvocatoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
