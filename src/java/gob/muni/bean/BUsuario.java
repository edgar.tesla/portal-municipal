/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bean;

/**
 *
 * @author COMPUTER
 */
public class BUsuario {

    private int cd_usuario;
    private String nm_usuario;
    private String ds_identidad;
    private String ds_clave;
    private String tp_rol;
    private String sn_activo;

    public BUsuario() {
    }

    public BUsuario(int cd_usuario, String nm_usuario, String ds_identidad, String ds_clave, String tp_rol, String sn_activo) {
        this.cd_usuario = cd_usuario;
        this.nm_usuario = nm_usuario;
        this.ds_identidad = ds_identidad;
        this.ds_clave = ds_clave;
        this.tp_rol = tp_rol;
        this.sn_activo = sn_activo;
    }

    public int getCd_usuario() {
        return cd_usuario;
    }

    public void setCd_usuario(int cd_usuario) {
        this.cd_usuario = cd_usuario;
    }

    public String getNm_usuario() {
        return nm_usuario;
    }

    public void setNm_usuario(String nm_usuario) {
        this.nm_usuario = nm_usuario;
    }

    public String getDs_identidad() {
        return ds_identidad;
    }

    public void setDs_identidad(String ds_identidad) {
        this.ds_identidad = ds_identidad;
    }

    public String getDs_clave() {
        return ds_clave;
    }

    public void setDs_clave(String ds_clave) {
        this.ds_clave = ds_clave;
    }

    public String getTp_rol() {
        return tp_rol;
    }

    public void setTp_rol(String tp_rol) {
        this.tp_rol = tp_rol;
    }

    public String getSn_activo() {
        return sn_activo;
    }

    public void setSn_activo(String sn_activo) {
        this.sn_activo = sn_activo;
    }

}
