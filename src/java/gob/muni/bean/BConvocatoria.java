/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bean;

/**
 *
 * @author COMPUTER
 */
public class BConvocatoria {

    private int cd_convocatoria;
    private String nm_convocatoria;
    private String ds_base;
    private String ds_link_base;
    private String ds_evaluacion;
    private String ds_link_evaluacion;
    private String ds_entrevista;
    private String ds_link_entrevista;
    private String ds_resultados;
    private String ds_link_resultados;

    public BConvocatoria() {
    }

    public BConvocatoria(int cd_convocatoria, String nm_convocatoria, String ds_base, String ds_link_base, String ds_evaluacion, String ds_link_evaluacion, String ds_entrevista, String ds_link_entrevista, String ds_resultados, String ds_link_resultados) {
        this.cd_convocatoria = cd_convocatoria;
        this.nm_convocatoria = nm_convocatoria;
        this.ds_base = ds_base;
        this.ds_link_base = ds_link_base;
        this.ds_evaluacion = ds_evaluacion;
        this.ds_link_evaluacion = ds_link_evaluacion;
        this.ds_entrevista = ds_entrevista;
        this.ds_link_entrevista = ds_link_entrevista;
        this.ds_resultados = ds_resultados;
        this.ds_link_resultados = ds_link_resultados;
    }

    public int getCd_convocatoria() {
        return cd_convocatoria;
    }

    public void setCd_convocatoria(int cd_convocatoria) {
        this.cd_convocatoria = cd_convocatoria;
    }

    public String getNm_convocatoria() {
        return nm_convocatoria;
    }

    public void setNm_convocatoria(String nm_convocatoria) {
        this.nm_convocatoria = nm_convocatoria;
    }

    public String getDs_base() {
        return ds_base;
    }

    public void setDs_base(String ds_base) {
        this.ds_base = ds_base;
    }

    public String getDs_link_base() {
        return ds_link_base;
    }

    public void setDs_link_base(String ds_link_base) {
        this.ds_link_base = ds_link_base;
    }

    public String getDs_evaluacion() {
        return ds_evaluacion;
    }

    public void setDs_evaluacion(String ds_evaluacion) {
        this.ds_evaluacion = ds_evaluacion;
    }

    public String getDs_link_evaluacion() {
        return ds_link_evaluacion;
    }

    public void setDs_link_evaluacion(String ds_link_evaluacion) {
        this.ds_link_evaluacion = ds_link_evaluacion;
    }

    public String getDs_entrevista() {
        return ds_entrevista;
    }

    public void setDs_entrevista(String ds_entrevista) {
        this.ds_entrevista = ds_entrevista;
    }

    public String getDs_link_entrevista() {
        return ds_link_entrevista;
    }

    public void setDs_link_entrevista(String ds_link_entrevista) {
        this.ds_link_entrevista = ds_link_entrevista;
    }

    public String getDs_resultados() {
        return ds_resultados;
    }

    public void setDs_resultados(String ds_resultados) {
        this.ds_resultados = ds_resultados;
    }

    public String getDs_link_resultados() {
        return ds_link_resultados;
    }

    public void setDs_link_resultados(String ds_link_resultados) {
        this.ds_link_resultados = ds_link_resultados;
    }

}
