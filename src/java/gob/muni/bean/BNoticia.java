/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.bean;

import java.io.File;
import java.io.InputStream;

/**
 *
 * @author COMPUTER
 */
public class BNoticia {

    private int cd_noticia;
    private String ds_titulo;
    private String ds_especificacion;
    private boolean sn_activo;
    private File im_noticia;
    private String ext_fotografia;
    private File pre_real_path_fotografia;
    private InputStream archivoimg;
    private byte[] archivoimg2;

    public BNoticia() {
    }

    public BNoticia(int cd_noticia, String ds_titulo, String ds_especificacion, boolean sn_activo, File im_noticia, String ext_fotografia, File pre_real_path_fotografia, InputStream archivoimg, byte[] archivoimg2) {
        this.cd_noticia = cd_noticia;
        this.ds_titulo = ds_titulo;
        this.ds_especificacion = ds_especificacion;
        this.sn_activo = sn_activo;
        this.im_noticia = im_noticia;
        this.ext_fotografia = ext_fotografia;
        this.pre_real_path_fotografia = pre_real_path_fotografia;
        this.archivoimg = archivoimg;
        this.archivoimg2 = archivoimg2;
    }

    public int getCd_noticia() {
        return cd_noticia;
    }

    public void setCd_noticia(int cd_noticia) {
        this.cd_noticia = cd_noticia;
    }

    public String getDs_titulo() {
        return ds_titulo;
    }

    public void setDs_titulo(String ds_titulo) {
        this.ds_titulo = ds_titulo;
    }

    public String getDs_especificacion() {
        return ds_especificacion;
    }

    public void setDs_especificacion(String ds_especificacion) {
        this.ds_especificacion = ds_especificacion;
    }

    public boolean isSn_activo() {
        return sn_activo;
    }

    public void setSn_activo(boolean sn_activo) {
        this.sn_activo = sn_activo;
    }

    public File getIm_noticia() {
        return im_noticia;
    }

    public void setIm_noticia(File im_noticia) {
        this.im_noticia = im_noticia;
    }

    public String getExt_fotografia() {
        return ext_fotografia;
    }

    public void setExt_fotografia(String ext_fotografia) {
        this.ext_fotografia = ext_fotografia;
    }

    public File getPre_real_path_fotografia() {
        return pre_real_path_fotografia;
    }

    public void setPre_real_path_fotografia(File pre_real_path_fotografia) {
        this.pre_real_path_fotografia = pre_real_path_fotografia;
    }

    public InputStream getArchivoimg() {
        return archivoimg;
    }

    public void setArchivoimg(InputStream archivoimg) {
        this.archivoimg = archivoimg;
    }

    public byte[] getArchivoimg2() {
        return archivoimg2;
    }

    public void setArchivoimg2(byte[] archivoimg2) {
        this.archivoimg2 = archivoimg2;
    }

}
