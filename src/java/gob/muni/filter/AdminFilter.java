/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.filter;

import gob.muni.bean.BUsuario;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author COMPUTER
 */
public class AdminFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("jejeje");
        BUsuario usuario = (BUsuario) ((HttpServletRequest) request).getSession().getAttribute("usuario");
        ServletContext context = request.getServletContext();
        System.out.println("contexto " + context.getContextPath());
        if (usuario != null && usuario.getTp_rol().equals("ADMINISTRADOR")) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse) response).sendRedirect(context.getContextPath() + "/forbidden.jsp");
        }
    }

    @Override
    public void destroy() {
    }

}
