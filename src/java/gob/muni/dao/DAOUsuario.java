/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.dao;

import gob.muni.bean.BUsuario;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.rx.cr.dao.DAOAbstract;

/**
 *
 * @author COMPUTER
 */
public class DAOUsuario extends DAOAbstract<BUsuario> {

    private BUsuario bean = null;
    private InputStream isc = null;

    public DAOUsuario(Connection cn) {
        setConnection(cn);
        setLista(new ArrayList<BUsuario>());
    }

    @Override
    public void registrar(BUsuario tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(BUsuario tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(BUsuario tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void busquedaAvanzada(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<BUsuario> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public BUsuario validarLogueoUsuario(String usuario, String clave) {
        try {
            clear();
            initExec("dbo.sp_validar_logueo(?,?)");
            setParameterString(1, usuario);
            setParameterString(2, clave);
            exec();
            conectionCommit();
            while (getResultSet().next()) {
                bean = new BUsuario();
                bean.setCd_usuario(getDataInt(1));
                bean.setNm_usuario(getDataString(2));
                bean.setTp_rol(getDataString(3));

            }
            return bean;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public BUsuario buscar(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
