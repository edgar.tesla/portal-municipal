/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.dao;

import gob.muni.bean.BNoticia;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.rx.cr.dao.DAOAbstract;
import org.rx.cr.util.Utilitarios;

/**
 *
 * @author COMPUTER
 */
public class DAONoticia extends DAOAbstract<BNoticia> {

    private BNoticia bean = null;
    private InputStream isc = null;

    public DAONoticia(Connection con) {
        setConnection(con);
        setLista(new ArrayList<BNoticia>());
    }

    @Override
    public void registrar(BNoticia bean) {
        try {
            initExec("dbo.sp_registrar_noticia(?,?,?)");
            setParameterString(1, bean.getDs_titulo());
            setParameterString(2, bean.getDs_especificacion());
            //setParameterBinaryStream(3, bean.getArchivoimg());
            try {
                bean.setPre_real_path_fotografia(new File(bean.getIm_noticia().getAbsolutePath()));
                bean.setIm_noticia(Utilitarios.encodeFileBinaryBASE64(bean.getIm_noticia(), bean.getIm_noticia().getAbsolutePath() + ".tmp"));
                System.out.println("hola mundo :" + bean.getIm_noticia().getAbsolutePath() + ".tmp");
                isc = new FileInputStream(bean.getIm_noticia());
                System.out.println("hola mundo : " + bean.getIm_noticia());
                System.out.println("hola mundo : " + isc);
                setParameterBinaryStream(3, isc, (int) bean.getIm_noticia().length());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
            }

            exec();
            conectionCommit();

            if (bean.getIm_noticia() != null && bean.getIm_noticia().exists()) {
                bean.getIm_noticia().delete();
                Utilitarios.deleteFile(bean.getIm_noticia().getAbsolutePath());
            }
            if (bean.getPre_real_path_fotografia() != null && bean.getPre_real_path_fotografia().exists()) {
                bean.getPre_real_path_fotografia().delete();
                Utilitarios.deleteFile(bean.getPre_real_path_fotografia().getAbsolutePath());
            }
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void actualizar(BNoticia bean) {
        try {
            initExec("dbo.sp_actualizar_noticia(?,?,?,?)");
            setParameterInt(1, bean.getCd_noticia());
            setParameterString(2, bean.getDs_titulo());
            setParameterString(3, bean.getDs_especificacion());
            //setParameterBinaryStream(3, bean.getArchivoimg());
            try {
                if (bean.getIm_noticia()!=null) {
                    bean.setPre_real_path_fotografia(new File(bean.getIm_noticia().getAbsolutePath()));
                    bean.setIm_noticia(Utilitarios.encodeFileBinaryBASE64(
                            bean.getIm_noticia(), bean.getIm_noticia().getAbsolutePath() + ".tmp"));
                    isc = new FileInputStream(bean.getIm_noticia());
                    setParameterBinaryStream(4, isc, (int) bean.getIm_noticia().length());
                }else{
                    byte array[]=new byte[bean.getArchivoimg().available()];
                    bean.getArchivoimg().read(array);
                    
                    byte arrayTemp[]=Utilitarios.encodeBinaryBASE64(array);
                    isc=new ByteArrayInputStream(arrayTemp);
                    setParameterBinaryStream(4, isc, isc.available());
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
            }

            exec();
            conectionCommit();

            if (bean.getIm_noticia() != null && bean.getIm_noticia().exists()) {
                bean.getIm_noticia().delete();
                Utilitarios.deleteFile(bean.getIm_noticia().getAbsolutePath());
            }
            if (bean.getPre_real_path_fotografia() != null && bean.getPre_real_path_fotografia().exists()) {
                bean.getPre_real_path_fotografia().delete();
                Utilitarios.deleteFile(bean.getPre_real_path_fotografia().getAbsolutePath());
            }
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void eliminar(BNoticia bean) {
        try {
            initExec("dbo.sp_eliminar_noticia(?)");
            setParameterInt(1, bean.getCd_noticia());            

            exec();
            conectionCommit();            
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void busquedaAvanzada(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<BNoticia> listar() {
        ArrayList<BNoticia> lista = new ArrayList<BNoticia>();
        try {
            clear();
            initExec("dbo.sp_listar_noticia()");
            exec();
            conectionCommit();
            while (getResultSet().next()) {
                bean = new BNoticia();
                bean.setCd_noticia(getDataInt(1));
                bean.setDs_titulo(getDataString(2));
                bean.setDs_especificacion(getDataString(3));
                try {
                    System.out.println("hola mano :" + getDataBytesStream(4).length);
                    bean.setIm_noticia(Utilitarios.decodeFileBASE64Binary(getDataBytesStream(4), ".png"));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                }
                bean.setSn_activo(getDataBoolean(5));

                lista.add(bean);
                System.out.println("lenght: " + lista.get(0).getIm_noticia().length());

            }
            return lista;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public BNoticia buscar(int i) {
        BNoticia bNoticia = null;
        try {
            clear();
            initExec("dbo.sp_obtener_noticia(?)");
            setParameterInt(1, i);
            exec();
            conectionCommit();
            while (getResultSet().next()) {
                bean = new BNoticia();
                bean.setCd_noticia(getDataInt(1));
                bean.setDs_titulo(getDataString(2));
                bean.setDs_especificacion(getDataString(3));

                try {
                    File file = Utilitarios.decodeFileBASE64Binary(getDataBytesStream(1), "imagen.png");
                    bean.setArchivoimg(new FileInputStream(file));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                }

                bNoticia = bean;
            }
            return bNoticia;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public InputStream obtenerImagenNoticia(int idNoticia) {
        InputStream is = null;
        File file = null;

        try {
            clear();
            initExec("dbo.sp_obtener_img_noticia(?)");
            setParameterInt(1, idNoticia);
            exec();
            conectionCommit();
            while (getResultSet().next()) {

                try {
                    System.out.println("tamaño de imagen: " + getDataBytesStream(1).length);
                    file = Utilitarios.decodeFileBASE64Binary(getDataBytesStream(1), "imagen.png");
                    is = new FileInputStream(file);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DAONoticia.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return is;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
