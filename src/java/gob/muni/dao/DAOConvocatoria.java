/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.dao;

import gob.muni.bean.BConvocatoria;
import gob.muni.bean.BNoticia;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.rx.cr.dao.DAOAbstract;
import org.rx.cr.util.Utilitarios;

/**
 *
 * @author COMPUTER
 */
public class DAOConvocatoria extends DAOAbstract<BConvocatoria> {

    private BConvocatoria bean = null;
    private InputStream isc = null;

    public DAOConvocatoria(Connection con) {
        setConnection(con);
        setLista(new ArrayList<BConvocatoria>());
    }

    @Override
    public void registrar(BConvocatoria bean) {
        try {
            initExec("dbo.sp_registrar_convocatoria(?,?,?,?,?,?,?,?,?)");
            setParameterString(1, bean.getNm_convocatoria());
            setParameterString(2, bean.getDs_base());
            setParameterString(3, bean.getDs_link_base());
            setParameterString(4, bean.getDs_evaluacion());
            setParameterString(5, bean.getDs_link_evaluacion());
            setParameterString(6, bean.getDs_entrevista());
            setParameterString(7, bean.getDs_link_entrevista());
            setParameterString(8, bean.getDs_resultados());
            setParameterString(9, bean.getDs_link_resultados());
            exec();
            conectionCommit();
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void actualizar(BConvocatoria bean) {
        try {
            initExec("dbo.sp_actualizar_convocatoria(?,?,?,?,?,?,?,?,?,?)");
            setParameterInt(1, bean.getCd_convocatoria());
            setParameterString(2, bean.getNm_convocatoria());
            setParameterString(3, bean.getDs_base());
            setParameterString(4, bean.getDs_link_base());
            setParameterString(5, bean.getDs_evaluacion());
            setParameterString(6, bean.getDs_link_evaluacion());
            setParameterString(7, bean.getDs_entrevista());
            setParameterString(8, bean.getDs_link_entrevista());
            setParameterString(9, bean.getDs_resultados());
            setParameterString(10, bean.getDs_link_resultados());
            exec();
            conectionCommit();
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void eliminar(BConvocatoria bean) {
        try {
            initExec("dbo.sp_eliminar_convocatoria(?)");
            setParameterInt(1, bean.getCd_convocatoria());
            exec();
            conectionCommit();
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void busquedaAvanzada(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<BConvocatoria> listar() {
        ArrayList<BConvocatoria> lista = new ArrayList<BConvocatoria>();
        try {
            clear();
            initExec("dbo.sp_listar_convocatoria()");
            exec();
            conectionCommit();
            while (getResultSet().next()) {
                bean = new BConvocatoria();
                bean.setCd_convocatoria(getDataInt(1));
                bean.setNm_convocatoria(getDataString(2));
                bean.setDs_base(getDataString(3));
                bean.setDs_link_base(getDataString(4));
                bean.setDs_evaluacion(getDataString(5));
                bean.setDs_link_evaluacion(getDataString(6));
                bean.setDs_entrevista(getDataString(7));
                bean.setDs_link_entrevista(getDataString(8));
                bean.setDs_resultados(getDataString(9));
                bean.setDs_link_resultados(getDataString(10));
                lista.add(bean);
            }
            return lista;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public BConvocatoria buscar(int i) {
        try {
            clear();
            initExec("dbo.sp_obtener_convocatoria(?)");
            setParameterInt(1, i);
            exec();
            conectionCommit();
            while (getResultSet().next()) {
                bean = new BConvocatoria();
                bean.setCd_convocatoria(getDataInt(1));
                bean.setNm_convocatoria(getDataString(2));
                bean.setDs_base(getDataString(3));
                bean.setDs_link_base(getDataString(4));
                bean.setDs_evaluacion(getDataString(5));
                bean.setDs_link_evaluacion(getDataString(6));
                bean.setDs_entrevista(getDataString(7));
                bean.setDs_link_entrevista(getDataString(8));
                bean.setDs_resultados(getDataString(9));
                bean.setDs_link_resultados(getDataString(10));
            }
            return bean;
        } catch (SQLException ex) {
            try {
                conectionRollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            ex.printStackTrace();
            return null;
        } finally {
            try {
                conectionClose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
