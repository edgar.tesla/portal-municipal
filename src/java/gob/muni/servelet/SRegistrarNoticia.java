/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.servelet;

import gob.muni.bean.BNoticia;
import gob.muni.bl.BLNoticia;
import gob.muni.util.Util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.rx.cr.util.Utilitarios;

/**
 *
 * @author COMPUTER
 */
public class SRegistrarNoticia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("op").equals("subir_fotografia_personal")) {
            subirFotografiaPersonal(request, response);
        } else if (request.getParameter("op").equals("previsualizar_fotografia_personal")) {
            obtenerURLFotografia(request, response);
        } else if (request.getParameter("op").equals("registrar_personal")) {
            if (request.getParameter("editar") == null || request.getParameter("editar") == "") {
                System.out.println("info editar " + request.getParameter("editar"));
                registrarNoticia(request, response);
            } else {
                editarNoticia(request, response);
            }
        } else if (request.getParameter("op").equals("fotografia_subida")) {
            fotografiaSubida(request, response);
        } else if (request.getParameter("op").equals("eliminar_noticia")) {
            eliminarNoticia(request, response);
        }
    }

    public void registrarNoticia(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        HttpSession session = request.getSession();
        InputStream inputStream = null;
        try {
            BNoticia bean = new BNoticia();
            /*Part file = request.getPart("im_noticia");
            System.out.println("controlador : "+file.getInputStream());
            inputStream = file.getInputStream();*/
            bean.setDs_titulo(request.getParameter("txt_titulo").trim().toLowerCase());
            bean.setDs_especificacion(request.getParameter("txt_especificacion").trim().toLowerCase());
            bean.setIm_noticia(new File(getServletContext().getRealPath("/Tmp") + File.separator + session.getAttribute("name_fotografia_personal").toString()));
            bean.setExt_fotografia(session.getAttribute("ext_fotografia_personal").toString());
            //bean.setArchivoimg(inputStream);

            BLNoticia bl = new BLNoticia(request);
            bl.guardarNoticia(bean);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Noticia Registrado Exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            out.print(ext);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se Pudo Registrar la noticia.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    public void editarNoticia(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        HttpSession session = request.getSession();
        InputStream inputStream = null;
        try {
            BNoticia bean = (BNoticia) session.getAttribute("noticia");

            bean.setDs_titulo(request.getParameter("txt_titulo").trim().toLowerCase());
            bean.setDs_especificacion(request.getParameter("txt_especificacion").trim().toLowerCase());

            if (session.getAttribute("name_fotografia_personal") != null) {
                bean.setIm_noticia(new File(getServletContext().getRealPath("/Tmp") + File.separator + session.getAttribute("name_fotografia_personal").toString()));
                bean.setExt_fotografia(session.getAttribute("ext_fotografia_personal").toString());
            }

            //bean.setArchivoimg(inputStream);
            BLNoticia bl = new BLNoticia(request);
            bl.editarNoticia(bean);
            session.setAttribute("noticia", null);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Noticia editada exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            ext.printStackTrace();
            out.print(ext);
            session.setAttribute("noticia", null);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se Pudo Registrar la noticia.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    public void fotografiaSubida(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("url_fotografia_personal") != null) {
                json.append("\"respt\":\"si\",");
                json.append("\"msg\":\"Fotografia Subida.\"})");
                out.println(json);
            } else {
                json.append("\"respt\":\"no\",");
                json.append("\"msg\":\"Falta Seleccionar una Fotogragia.\"})");
                out.println(json);
            }
        } finally {
            out.close();
        }

    }

    public void obtenerURLFotografia(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        StringBuilder json = new StringBuilder("({");
        if (session.getAttribute("url_fotografia_personal") != null) {
            json.append("\"url_fotografia_personal\":\"").append(session.getAttribute("url_fotografia_personal")).append("\"})");
            out.println(json);
        } else {
            String dafault_url_fotografia_personal = request.getContextPath() + "/web/imagenes/logo.png";
            json.append("\"url_fotografia_personal\":\"").append(dafault_url_fotografia_personal).append("\"})");
            out.println(json);
        }
    }

    public void subirFotografiaPersonal(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException {
        String tmp_dir = getServletContext().getRealPath("/Tmp") + File.separator;
        File fl = new File(tmp_dir);
        if (!fl.exists()) {
            fl.mkdir();
        }
        fl = subirArchivo(request, response, tmp_dir);
        if (fl != null) {
            HttpSession session = request.getSession();
            if (fl.exists()) {
                session.setAttribute("url_fotografia_personal", request.getContextPath() + "/Tmp/" + fl.getName());
                session.setAttribute("url_real_fotografia_personal", fl.getAbsolutePath());
                session.setAttribute("name_fotografia_personal", fl.getName());
                session.setAttribute("ext_fotografia_personal", Utilitarios.getExtencion(fl));
            } else {
                session.setAttribute("url_fotografia_personal", null);
                session.setAttribute("url_real_fotografia_personal", null);
                session.setAttribute("name_fotografia_personal", null);
                session.setAttribute("ext_fotografia_personal", null);
            }
        }
    }

    public File subirArchivo(HttpServletRequest request, HttpServletResponse response, String real_path) throws FileNotFoundException {
        PrintWriter writer = null;
        InputStream is = null;
        FileOutputStream fos = null;
        File archivo_subido = null;
        try {
            writer = response.getWriter();
        } catch (IOException ex) {
        }

        String nombre_archivo = "fotografia_personal_" + request.getSession().getId() + Util.getExtencion(request.getHeader("X-File-Name"));
        try {
            is = request.getInputStream();
            archivo_subido = new File(real_path + nombre_archivo);
            fos = new FileOutputStream(archivo_subido);
            byte[] data_buffer = new byte[4096];
            int n = 0;
            while (-1 != (n = is.read(data_buffer))) {
                fos.write(data_buffer, 0, n);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            writer.print("{success: true}");
            return archivo_subido;
        } catch (FileNotFoundException ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
            return null;
        } catch (IOException ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
            return null;
        } finally {
            try {
                fos.close();
                is.close();
            } catch (IOException ex) {
            }
            writer.flush();
            writer.close();
        }
    }

    public void eliminarNoticia(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        try {
            BNoticia bean = new BNoticia();
            bean.setCd_noticia(Integer.parseInt(request.getParameter("id_noticia").trim()));

            //bean.setArchivoimg(inputStream);
            BLNoticia bl = new BLNoticia(request);
            bl.eliminarNoticia(bean);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Noticia eliminada exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            out.print(ext);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se Pudo eliminar la noticia.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
