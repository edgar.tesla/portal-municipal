/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.servelet;

import gob.muni.bean.BUsuario;
import gob.muni.bl.BLUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import javafx.scene.effect.BlurType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.rx.cr.util.Utilitarios;

/**
 *
 * @author COMPUTER
 */
public class SIniciarSession extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("op").equals("validar_logueo")) {
            validarLogueoPersonal(request, response);
        }
    }

    public void validarLogueoPersonal(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        try {
            BLUsuario bl = new BLUsuario(request);
            System.out.println("aquii el error!!!!");
            BUsuario usuario = bl.validadLogueUsuario(request.getParameter("txt_usuario").trim(), request.getParameter("txt_clave").trim());
            System.out.println("hola mundo : "+usuario.getNm_usuario());
            if (usuario != null) {
                HttpSession session = request.getSession();
                session.setAttribute("id_personal", usuario.getCd_usuario());
                session.setAttribute("codigo_personal", usuario.getNm_usuario());
                session.setAttribute("cargo_personal", usuario.getTp_rol());
                session.setAttribute("usuario", usuario);
                json.append("\"respt\":\"exito\",");
                json.append("\"pagina\":\"administrador/admin.jsp\",");
                json.append("\"msg\":\"¡Bienvenido! ").append(usuario.getNm_usuario().toUpperCase()).append("\"})");
                out.println(json);
            } else {
                json.append("\"respt\":\"error\",");
                json.append("\"msg\":\"Datos Incorrectos.\"})");
                out.println(json);
            }
        } catch (Exception ext) {
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"Error en Servidor.\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
