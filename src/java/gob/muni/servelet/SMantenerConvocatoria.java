/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.servelet;

import gob.muni.bean.BConvocatoria;
import gob.muni.bl.BLConvocatoria;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.rx.cr.util.Utilitarios;

/**
 *
 * @author COMPUTER
 */
public class SMantenerConvocatoria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("op").equals("listar_convocatoria")) {
            listarConvocatoria(request, response);
        } else if (request.getParameter("op").equals("listar_convocatoria_admin")) {
            listarConvocatoriaAdmin(request, response);
        } else if (request.getParameter("op").equals("recuperar_datos_convocatoria")) {
            obtenerDatosConvocatoria(request, response);
        } else if (request.getParameter("op").equals("modificar_datos_convocatoria")) {
            actualizarConvocatoria(request, response);
        }else if (request.getParameter("op").equals("eliminar_convocatoria")) {
            eliminarConvocatoria(request, response);
        }
        
    }
    
    public void actualizarConvocatoria(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        HttpSession session = request.getSession();
        InputStream inputStream = null;
        try {
            BLConvocatoria bl = new BLConvocatoria(request);
            BConvocatoria bean = new BConvocatoria();
            bean.setCd_convocatoria(Integer.parseInt(request.getParameter("txt_idconvocatoria").trim()));
            bean.setNm_convocatoria(request.getParameter("txt_nombre").trim());
            bean.setDs_base(request.getParameter("txt_based").trim());
            bean.setDs_link_base(request.getParameter("txt_basel").trim());
            bean.setDs_evaluacion(request.getParameter("txt_evaluaciond").trim());
            bean.setDs_link_evaluacion(request.getParameter("txt_evaluacionl").trim());
            bean.setDs_entrevista(request.getParameter("txt_entrevistad").trim());
            bean.setDs_link_entrevista(request.getParameter("txt_entrevistal").trim());
            bean.setDs_resultados(request.getParameter("txt_resultadod").trim());
            bean.setDs_link_resultados(request.getParameter("txt_resultadol").trim());
            System.out.println("hola ggegege");
            bl.actualizarConvocatoria(bean);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Convocatoria Modificado Exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            out.print(ext);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se Pudo Modificar la Convocatoria.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }
    
    public void obtenerDatosConvocatoria(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        String ref = request.getParameter("id_convocatoria").trim();
        System.out.println("ide serv :" + ref);
        System.out.println("jejejeje");
        try {
            int id_proveedor = Integer.parseInt(ref);
            BLConvocatoria bl = new BLConvocatoria(request);
            BConvocatoria bean = bl.buscarConvocatoria(id_proveedor);
            System.out.println("nombre es : " + bean.getNm_convocatoria());
            json.append("\"id_convocatoria\":\"").append(bean.getCd_convocatoria()).append("\",");
            json.append("\"ds_nombre\":\"").append(bean.getNm_convocatoria()).append("\",");
            json.append("\"ds_based\":\"").append(bean.getDs_base()).append("\",");
            json.append("\"ds_basel\":\"").append(bean.getDs_link_base()).append("\",");
            json.append("\"ds_evaluaciond\":\"").append(bean.getDs_evaluacion()).append("\",");
            json.append("\"ds_evaluacionl\":\"").append(bean.getDs_link_evaluacion()).append("\",");
            json.append("\"ds_entrevistad\":\"").append(bean.getDs_entrevista()).append("\",");
            json.append("\"ds_entrevistal\":\"").append(bean.getDs_link_entrevista()).append("\",");
            json.append("\"ds_resultadod\":\"").append(bean.getDs_resultados()).append("\",");
            json.append("\"ds_resultadol\":\"").append(bean.getDs_link_resultados()).append("\"})");
            System.out.println("hhggfgfhhh :" + json);
            out.println(json);
            
        } catch (Exception ext) {
            json.append("\"id_convocatoria\":\"").append("").append("\",");
            json.append("\"ds_nombre\":\"").append("").append("\",");
            json.append("\"ds_based\":\"").append("").append("\",");
            json.append("\"ds_basel\":\"").append("").append("\",");
            json.append("\"ds_evaluaciond\":\"").append("").append("\",");
            json.append("\"ds_evaluacionl\":\"").append("").append("\",");
            json.append("\"ds_entrevistad\":\"").append("").append("\",");
            json.append("\"ds_entrevistal\":\"").append("").append("\",");
            json.append("\"ds_resultadod\":\"").append("").append("\",");
            json.append("\"ds_resultadol\":\"").append("").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
        
    }
    
    public void listarConvocatoria(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String html_row_table = "";
        try {
            BLConvocatoria bl = new BLConvocatoria(request);
            ArrayList<BConvocatoria> list = bl.listarConvocatoria();
            if (list.size() > 0) {
                for (int i=0;i<list.size();i++) {
                    html_row_table += "<tr id=\"" + list.get(i).getCd_convocatoria() + "\" onclick=\"obtenerConvocatoria(this.id)\" >\n";
                    html_row_table += "<td>" + list.get(i).getNm_convocatoria() + "</td>\n";
                    html_row_table += "<td>" + "<a target=\"_blank\" href=" + list.get(i).getDs_link_base() + ">" + (!(list.get(i).getDs_link_base().length()>0)?"":"<i class=\"fas fa-file-pdf fa-7x\"></i>") + "</a>" + "</td>\n";
                    html_row_table += "<td>" + "<a target=\"_blank\" href=" + list.get(i).getDs_link_evaluacion() + ">" + (!(list.get(i).getDs_link_evaluacion().length()>0)?"":"<i class=\"far fa-file-pdf fa-7x\"></i>") + "</a>" + "</td>\n";
                    html_row_table += "<td>" + "<a target=\"_blank\" href=" + list.get(i).getDs_link_entrevista() + ">" + (!(list.get(i).getDs_link_entrevista().length()>0)?"":"<i class=\"far fa-file-pdf fa-7x\"></i>") + "</a>" + "</td>\n";
                    html_row_table += "<td>" + "<a target=\"_blank\" href=" + list.get(i).getDs_link_resultados() + ">" + (!(list.get(i).getDs_link_resultados().length()>0)?"":"<i class=\"far fa-file-pdf fa-7x\"></i>") + "</a>" + "</td>\n";
                    html_row_table += "</tr>\n";
                }
            } else {
                html_row_table += "<tr id=\"tr0\" class=\"row_table_select\">";
                html_row_table += "<td colspan=\"5\" class=\"grid_body_default\">" + "Ninguna Coensidencia Encontrada!" + "</td>";
                html_row_table += "</tr>";
            }
            out.print(html_row_table);
            request.setAttribute("list", list);
        } catch (Exception ext) {
            out.write(ext.toString());
        } finally {
            out.close();
        }
    }
    
    public void listarConvocatoriaAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String html_row_table = "";
        try {
            BLConvocatoria bl = new BLConvocatoria(request);
            ArrayList<BConvocatoria> list = bl.listarConvocatoria();
            System.out.println("tamaño :" + list.size());
            if (list.size() > 0) {
                for (BConvocatoria bean : list) {
                    html_row_table += "<tr id=\"" + bean.getCd_convocatoria() + "\"  >\n";
                    html_row_table += "<td>" + bean.getNm_convocatoria() + "</td>\n";
                    html_row_table += "<td>" + "<a href=" + bean.getDs_link_base() + ">" + bean.getDs_base() + "</a>" + "</td>\n";
                    html_row_table += "<td>" + "<center><a onclick=\"obtenerConvocatoria("+bean.getCd_convocatoria()+")\" class=\"caption lfl btn btn-primary btn_theme\">" + "Editar" + "</a></center>" + "</td>\n";
                    html_row_table += "<td>" + "<center><a onclick=\"mostrarDialogConvocatoria("+bean.getCd_convocatoria()+")\" class=\"caption lfl btn btn-secondary btn_theme\">" + "Eliminar" + "</a></center>" + "</td>\n";
                    html_row_table += "</tr>\n";
                }
            } else {
                html_row_table += "<tr id=\"tr0\" class=\"row_table_select\">";
                html_row_table += "<td colspan=\"3\" class=\"grid_body_default\">" + "Ninguna Coensidencia Encontrada!" + "</td>";
                html_row_table += "</tr>";
            }
            out.print(html_row_table);
            request.setAttribute("list", list);
        } catch (Exception ext) {
            out.write(ext.toString());
        } finally {
            out.close();
        }
    }
    public void eliminarConvocatoria(HttpServletRequest request,HttpServletResponse response) throws IOException{
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        try {
            BLConvocatoria bl = new BLConvocatoria(request);
            BConvocatoria bean = new BConvocatoria();
            bean.setCd_convocatoria(Integer.parseInt(request.getParameter("id_convocatoria").trim()));
            
            bl.eliminarConvocatoria(bean);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Convocatoria eliminada exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            out.print(ext);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se pudo eliminar la convocatoria.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
