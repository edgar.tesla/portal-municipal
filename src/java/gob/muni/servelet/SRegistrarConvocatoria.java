/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gob.muni.servelet;

import gob.muni.bean.BConvocatoria;
import gob.muni.bl.BLConvocatoria;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author COMPUTER
 */
public class SRegistrarConvocatoria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("op").equals("registrar_convocatoria")) {
            registrarConvocatoria(request, response);
        }
    }

    public void registrarConvocatoria(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder json = new StringBuilder("({");
        HttpSession session = request.getSession();
        InputStream inputStream = null;
        try {
            BLConvocatoria bl = new BLConvocatoria(request);
            BConvocatoria bean = new BConvocatoria();
            bean.setNm_convocatoria(request.getParameter("txt_nombre").trim());
            bean.setDs_base(request.getParameter("txt_based").trim());
            bean.setDs_link_base(request.getParameter("txt_basel").trim());
            bean.setDs_evaluacion(request.getParameter("txt_evaluaciond").trim());
            bean.setDs_link_evaluacion(request.getParameter("txt_evaluacionl").trim());
            bean.setDs_entrevista(request.getParameter("txt_entrevistad").trim());
            bean.setDs_link_entrevista(request.getParameter("txt_entrevistal").trim());
            bean.setDs_resultados(request.getParameter("txt_resultadod").trim());
            bean.setDs_link_resultados(request.getParameter("txt_resultadol").trim());
            bl.guardarConvocatoria(bean);
            json.append("\"respt\":\"exito\",");
            json.append("\"msg\":\"Convocatoria Registrado Exitosamente.\"})");
            out.println(json);
        } catch (Exception ext) {
            out.print(ext);
            json.append("\"respt\":\"error\",");
            json.append("\"msg\":\"").append("No se Pudo Registrar la Convocatoria.").append("\"})");
            out.println(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
