package gob.muni.util;

public class Util {
    public static java.sql.Date convertStringToDate(String referencia){
        int anio,mes,dia,post;
        post = referencia.indexOf("/")!=-1?referencia.indexOf("/"):referencia.indexOf("-");
        dia = Integer.parseInt(referencia.substring(0,post).trim());
        referencia = referencia.substring(post+1,referencia.length());
        post = referencia.indexOf("/")!=-1?referencia.indexOf("/"):referencia.indexOf("-");
        mes = Integer.parseInt(referencia.substring(0,post).trim());
        referencia = referencia.substring(post+1,referencia.length());
        anio = Integer.parseInt(referencia.trim());         
        return new java.sql.Date(anio-1900,mes-1,dia);
    }
    public static String getExtencion(String str){
        return str.lastIndexOf(".")!=-1?str.substring(str.lastIndexOf("."),str.length()):"";        
    }
}
